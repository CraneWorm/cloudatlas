from nose.tools import *
import operator as op
from interpreter.value import *


import random

class TestValue(object):

    def test_value(self):
        assert_equal(ValueInt(1) + ValueInt(41), ValueInt(42))
        assert_equal(ValueInt(1) + ValueInt(41) == ValueInt(42), ValueBoolean(True))
        assert_not_equal(ValueInt(1) * ValueInt(41) == ValueInt(42), ValueBoolean(True))
        assert_equal(ValueDouble(1) + ValueDouble(41), ValueDouble(42))
        assert_equal(ValueDouble(1) + ValueDouble(41) == ValueDouble(42), ValueBoolean(True))
        assert_equal(ValueDouble(43) - ValueDouble(1), ValueDouble(42))
        assert_equal(ValueDouble(3) * ValueDouble(7), ValueDouble(42) / ValueDouble(2))
        assert_equal(ValueDouble(42) % ValueDouble(11), ValueDouble(9))
        assert_equal(ValueInt(42) / ValueInt(12), ValueDouble(3.5))
        assert_equal(ValueDouble(3.5), ValueInt(42) / ValueInt(12))
        assert_not_equal(ValueDouble(3.5), ValueInt(42) / ValueInt(11))
        assert_not_equal(ValueDouble(3.5) == ValueDouble(3.5), ValueBoolean(False))



    def test_valueTime(self):
        t1, t2 = ValueTime('2014/10/31 20:00:40.150'), ValueTime('2013/10/31 20:00:40.150')
        td1, td2 = t2 - t1, t1 - t2
        assert_equal(t1 + td1, t2)
        assert_equal(t2 + td2, t1)

    def test_valueContainers(self):
        s1 = ValueSet({ValueInt(1), ValueInt(4), ValueInt(9)})
        s2 = ValueSet({ValueInt(1), ValueInt(4), ValueInt(9)})
        s3 = ValueSet({ValueInt(1), ValueInt(4)})

        assert_equal(s1, s2)
        assert_equal(s1, s1 + s2)
        assert_not_equal(s1, s3)
        assert_equal(s1 + s3, s1)
        assert_raises(TypeError, op.add, s1, ValueInt(1))
        assert_equals(s1 + ValueNone(), ValueNone())

        l1 = ValueList([ValueInt(1), ValueInt(4), ValueInt(9)])
        l2 = ValueList([ValueInt(1), ValueInt(4), ValueInt(9)])
        l3 = ValueList([ValueInt(1), ValueInt(4)])

        assert_equal(l1, l2)
        assert_equal(l1 + l1, l1 + l2)
        assert_not_equal(l1, l3)
        assert_equal(l1 + l3, l2 + l3)
class TestResultValues(object):

    def setup(self):
        self.col1 = ColumnResult([ValueInt(x) for x in random.sample(range(1000), 100)])
        self.col2 = ColumnResult([ValueInt(x) for x in random.sample(range(1000), 100)])
        self.listR = ListResult([ValueInt(1) for x in range(99)])
        self.oneRes = OneResult(ValueInt(1))
        self.none = OneResult(ValueNone())

    def test_values(self):
        def assertAllTrue(column):
            assert(all([isinstance(vBool, ValueBoolean) for vBool in column.val]))
            assert(all([vBool.val for vBool in column.val]))

        def assertAllNone(column):
            assert(all([isinstance(v, ValueNone) for v in column.val]))

        col3 = self.col1 + self.col2
        colBool = col3 == ColumnResult([ValueInt(op.add(a, b).val) for (a, b) in zip(self.col1.val, self.col2.val)])
        assertAllTrue(colBool)
        assertAllTrue(col3 + self.oneRes == self.oneRes + col3)
        assertAllTrue(self.listR == self.oneRes)

        assertAllNone(self.listR + self.none)