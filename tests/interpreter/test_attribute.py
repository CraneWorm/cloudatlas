from nose.tools import *
from interpreter.attribute import *

class TestAttribute(object):

    def setup(self):
        self.attr1 = Attribute("attr_one")
        self.attr2 = Attribute("&attr_query")

    def test_name(self):
        assert_equal(self.attr1.name, "attr_one")
        assert_equal(self.attr2.name, "&attr_query")

    def test_isQuery(self):
        assert_equal(self.attr1.isQuery, False)
        assert_equal(self.attr2.isQuery, True)

    def test_equals(self):
        assert_equal(self.attr1.equals(self.attr1), True)
        assert_equal(self.attr1.equals(self.attr2), False)

