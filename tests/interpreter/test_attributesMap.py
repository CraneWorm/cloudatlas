from nose.tools import *
from interpreter.attribute import *
from interpreter.attributesMap import *
from interpreter import value

class TestAttribute(object):

    def setup(self):
        self.attrMap = AttributesMap()
        self.attr1 = Attribute("one")
        self.attr2 = Attribute("two")

    def test_map(self):
        assert_equal(self.attrMap.get(self.attr1), value.ValueNone())
        self.attrMap.add(self.attr1, "val_one")
        self.attrMap.add(self.attr2, "val_two")
        assert_equal(self.attrMap.get(self.attr1), "val_one")
        assert_equal(self.attrMap.get(self.attr2), "val_two")

