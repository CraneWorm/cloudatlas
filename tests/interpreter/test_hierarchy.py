from nose.tools import *
from interpreter.ZMI import *
from interpreter.value import *
from interpreter.hierarchy import *

class TestHierarchy(object):

    def setup(self):
        self.hierarchy = Hierarchy()

    def test_fill(self):
        self.hierarchy.fill()
        assert_equal(len(self.hierarchy.root.children), 2)
        assert_equal(len(self.hierarchy.uw.children), 3)
        assert_equal(len(self.hierarchy.pjwstk.children), 2)

