from nose.tools import *
from interpreter.parser import visitor as V

class Lion: pass
class Tiger: pass
class Bear: pass


class ZooVisitor:

    @V.on('animal')
    def visit(self, animal):
       """this methods purpose is to
       initialize the method dispatcher
       """

    @V.when(Lion)
    def visit(self, animal):
        return "Lions"

    @V.when(Tiger)
    def visit(self, animal):
        return "tigers"

    @V.when(Bear)
    def visit(self, animal):
        return "and bears, oh my!"


class TestVisitor(object):

    def test_ZooVisitor(self):
        animals = [Lion(), Tiger(), Bear()]
        visitor = ZooVisitor()
        assert_equal(', '.join(visitor.visit(animal) for animal in animals), 'Lions, tigers, and bears, oh my!')
