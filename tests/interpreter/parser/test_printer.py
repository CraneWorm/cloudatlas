from nose.tools import *
from interpreter.parser.parser import *
from interpreter.parser.lexer import *
from interpreter.parser.printer import QueryPrinter
import logging


class TestPrinter(object):

    def setup(self):
        self.parser = Parser(outputdir='tests/interpreter/parser')
        self.lexer = Lexer(debug=0)

    def test_printQuery(self):
        self.lexer.input(r'SELECT a OR b REGEXP "" AS c ORDER BY sum(b) DESC, a + 9 ASC NULLS FIRST')
        printer = QueryPrinter()
        result = self.parser.parse(lexer=self.lexer, debug=0)
        print printer.visit(*result)
