from nose.tools import *
from interpreter.parser.lexer import *


class TestLexer(object):

    def setup(self):
        self.lexer = Lexer()

    def test_simpleTokens(self):
        data = """\
1 10 1.3 .5 103e-0 10E-1 39.2e1 .5e4
SELECT AS WHERE BY ORDER
+-<><=>==><,;NULLS true false truefalse
ident_ i1dent iNDent iASb 2a
WHEREOR ANDSELECT ASBY
"""
        self.lexer.input(data)
        values = [('NUMBER', ValueInt(1)),
                  ('NUMBER', ValueInt(10)),
                  ('DOUBLE', ValueDouble(1.3)),
                  ('DOUBLE', ValueDouble(0.5)),
                  ('DOUBLE', ValueDouble(103.0)),
                  ('DOUBLE', ValueDouble(1.0)),
                  ('DOUBLE', ValueDouble(392.0)),
                  ('DOUBLE', ValueDouble(5000.0)),
                  ('SELECT', 'SELECT'),
                  ('AS', 'AS'),
                  ('WHERE', 'WHERE'),
                  ('BY', 'BY'),
                  ('ORDER', 'ORDER'),
                  ('PLUS', '+'),
                  ('MINUS', '-'),
                  ('NE', '<>'),
                  ('LE', '<='),
                  ('GE', '>='),
                  ('EQ', '='),
                  ('GT', '>'),
                  ('LT', '<'),
                  ('COMA',','),
                  ('SEMI', ';'),
                  ('NULLS', 'NULLS'),
                  ('BOOL', ValueBoolean(True)),
                  ('BOOL', ValueBoolean(False)),
                  ('NAME', 'truefalse'),
                  ('NAME', 'ident_'),
                  ('NAME', 'i1dent'),
                  ('NAME', 'iNDent'),
                  ('NAME', 'iASb'),
                  ('NUMBER', ValueInt(2)),
                  ('NAME', 'a'),
                  ('NAME', 'WHEREOR'),
                  ('NAME', 'ANDSELECT'),
                  ('NAME', 'ASBY')]
        for ((t1, v1), (t2, v2)) in zip(values, [(t.type, t.value) for t in self.lexer]):
            assert_equals(t1, t2)
            cmpr = (v1 == v2)
            assert(isinstance(cmpr, ValueBoolean) and isinstance(cmpr.val, bool) and cmpr.val
                   or isinstance(cmpr, bool) and cmpr)

    def test_stringLiterals(self):
        data = r'"literal AS OR" SELECT"no\t \"ident \n\t\"" yes_ident'
        self.lexer.input(data)
        values = [('STRING', ValueString(r'literal AS OR')),
                  ('SELECT', 'SELECT'),
                  ('STRING', ValueString(r'no\t \"ident \n\t\"')),
                  ('NAME', "yes_ident")]
        for ((t1, v1), (t2, v2)) in zip(values, [(t.type, t.value) for t in self.lexer]):
            assert_equals(t1, t2)
            cmpr = (v1 == v2)
            assert(isinstance(cmpr, ValueBoolean) and isinstance(cmpr.val, bool) and cmpr.val
                   or isinstance(cmpr, bool) and cmpr)