from nose.tools import *
from interpreter.parser.parser import *
from interpreter.parser.lexer import *
import logging

class TestParser(object):

    def setup(self):
        self.parser = Parser(outputdir='tests/interpreter/parser')
        self.lexer = Lexer(debug=0)

    def test_parseEmpty(self):
        self.lexer.input(r'SELECT a OR b REGEXP "" AS c ORDER BY c DESC, a ASC NULLS FIRST')
        result = self.parser.parse(lexer=self.lexer, debug=0)
        print result
