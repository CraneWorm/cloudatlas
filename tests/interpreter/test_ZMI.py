from nose.tools import *
from interpreter.ZMI import *

class TestZMI(object):

    def setup(self):
        self.root = ZMI(None)
        self.child1 = ZMI(parent=self.root)
        self.child2 = ZMI(parent=self.root)

    def test_parent(self):
        assert_equal(self.child1.parent, self.child2.parent)
        assert_equal(len(self.root.children), 2)
        self.child2.parent = self.child1
        assert_equal(self.child1, self.child2.parent)
        assert_equal(len(self.root.children), 1)

#    def test_level(self):
#        assert_equal(self.child1.level, self.child2.level)
#        assert_equal(self.root.level, 0)
#        self.child2.parent = self.child1
#        assert_equal(self.child2.level, 2)
#        self.child2.parent = "ABSURD"
#        assert_equal(self.child2.level,0)
