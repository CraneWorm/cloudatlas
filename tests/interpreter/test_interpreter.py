from nose.tools import *
from interpreter.parser.parser import *
from interpreter.parser.lexer import *
from interpreter.interpreter import QueryInterpreter, RuleNotImplemented
from interpreter.hierarchy import Hierarchy
import logging

class TestInterpreter(object):

    def test_interpreter(self):
        parser = Parser(outputdir='tests/interpreter/parser')
        lexer = Lexer(debug=0)
        hierarchy = Hierarchy()
        hierarchy.fill()
        interpreter = QueryInterpreter()

        query_strings = [
            r'SELECT 2 + 2 AS two_plus_two',
            r'SELECT to_integer((to_double(3) - 5.6) / 11.0 + to_double(47 * (31 - 15))) AS math WHERE true',
            r'SELECT count(members) AS members_count',
            r'SELECT first(99, name) AS new_contacts ORDER BY cpu_usage DESC NULLS LAST, num_cores ASC NULLS FIRST',
            r'SELECT count(num_cores - size(some_names)) AS sth',
            r'SELECT min(sum(distinct(2 * level)) + 38 * size(contacts)) AS sth WHERE num_cores < 8',
            r'SELECT first(1, name) + last(1,name) AS concat_name WHERE num_cores >= (SELECT min(num_cores) ORDER BY timestamp) ORDER BY creation ASC NULLS LAST',
            r'SELECT sum(cardinality) AS cardinality',
            r'SELECT land(cpu_usage < 0.5) AS cpu_ok',
            r'SELECT min(name) AS min_name, to_string(first(1, name)) AS max_name ORDER BY name DESC',
            r'SELECT epoch() AS epoch, land(timestamp > epoch()) AS afterY2K',
            r'SELECT min(timestamp) + (max(timestamp) - epoch()) / 2 AS t2',
            r'SELECT lor(unfold(some_names) + "xx" REGEXP "([a-z]*)atkax([a-z]*)") AS beatka',
            r'SELECT (SELECT avg(cpu_usage) WHERE false) AS smth',
            r'SELECT avg(cpu_usage) AS cpu_usage WHERE (SELECT sum(cardinality)) > (SELECT to_integer((1 + 2 + 3 + 4) / 5))',
            r'SELECT ceil(to_double(min(num_cores)) / 1.41) AS sth',
            r'SELECT floor(5.0 / 1.9) AS fl',
            r'SELECT to_time("2013/07/05 12:54:32.098") + to_duration(6811) AS tim',
            r'SELECT avg(cpu_usage * to_double(num_cores)) AS cpu_load, sum(num_cores) AS num_cores',
            r'SELECT first(3, unfold(contacts)) AS contacts'
        ]

        query_results = [
            [[(OneResult(ValueInt(4)), 'two_plus_two')]],
            [[(OneResult(ValueInt(751)), 'math')]],
            [[(OneResult(ValueInt(3)), 'members_count')]],
            [[(OneResult(ValueList([ValueString('khaki13'), ValueString('violet07'), ValueString('khaki31')])), 'new_contacts')]],
            [[(OneResult(ValueInt(2)), 'sth')]],
            [[(OneResult(ValueInt(80)), 'sth')]],
            [[(OneResult(ValueList([ValueString('violet07'), ValueString('khaki31')])), 'concat_name')]],
            [[(OneResult(ValueInt(3)), 'cardinality')]],
            [[(OneResult(ValueBoolean(False)), 'cpu_ok')]],
            [[(OneResult(ValueString('khaki13')), 'min_name'), (OneResult(ValueString('[violet07]')), 'max_name')]],
            [[(OneResult(ValueTime('2000/01/01 00:00:00.000')), 'epoch'), (OneResult(ValueBoolean(True)), 'afterY2K')]],
            [[(OneResult(ValueTime('2019/04/16 04:31:30.000')), 't2')]],
            [[(OneResult(ValueBoolean(True)), 'beatka')]],
            [[(OneResult(ValueNone()), 'smth')]],
            [[(OneResult(ValueDouble(0.5)), 'cpu_usage')]],
            [[(OneResult(ValueDouble(3.0)), 'sth')]],
            [[(OneResult(ValueDouble(2.0)), 'fl')]],
            [[(OneResult(ValueTime('2013/07/05 12:54:38.909')), 'tim')]],
            [[(OneResult(ValueDouble(2.7)), 'cpu_load'), (OneResult(ValueInt(6)), 'num_cores')]],
            [[(OneResult(ValueList([ValueString("UW1C"), ValueString("UW1A"), ValueString("UW1B")])), 'contacts')]]
        ]

        results = []

        for query_string in query_strings:
            lexer.input(query_string)
            query = parser.parse(lexer=lexer, debug=0)
            result = interpreter(hierarchy.uw, (None, query_string), query)
            results.append(result)

        assert_equals(query_results, results)
