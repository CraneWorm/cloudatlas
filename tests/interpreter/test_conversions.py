from nose.tools import *
from interpreter.conversions import Conversions
from interpreter.value import *


class test_Conversions(object):

    def test_simpleConversions(self):
        one = ValueInt(1)
        forty_two_and_a_half = ValueDouble(42.5)
        C = Conversions()
        assert_equal(one, C('to_integer', C('to_double',one)))
        assert_equal(one, C('to_integer', C('to_string', one)))
        assert_equal(forty_two_and_a_half, C('to_double', C('to_string', forty_two_and_a_half)))

        date1 = C('to_time', C('to_string', ValueTime("2014/11/03 02:23:28.000")))
        date2 = C('to_time', C('to_string', ValueTime("2013/11/10 02:23:28.000")))
        duration1 = C('to_duration', C('to_string', C('to_duration', ValueInt(365 * 24 * 3600 * 10**3 - 7 * 24 * 3600 * 10**3))))
        duration2 = C('to_duration', C('to_string', C('to_duration', ValueInt(-365 * 24 * 3600 * 10**3 + 7 * 24 * 3600 * 10**3))))

        assert_equal(date1 - date2, duration1)
        assert_equal(date2 - date1, duration2)

        true = ValueBoolean(True)
        false = ValueString('false')

        assert_equal(true, C('to_boolean', C('to_string', true)))
        assert_equal(false, C('to_string', C('to_boolean', false)))