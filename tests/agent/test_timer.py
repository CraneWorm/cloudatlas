import time
import pykka
from agent.timer import Timer


class Tester(pykka.ThreadingActor):
    def __init__(self, *args, **kwargs):
        super(Tester, self).__init__(*args, **kwargs)
        self.msgs = []

    def print_message(self, message):
        print(message)
        self.msgs.append(message)

    def check_message(self):
        return ' '.join(self.msgs) == 'This sentence is synchronized .'


def test_timer():
    tester = Tester.start().proxy()
    timer = Timer.start().proxy()
    timer.set_timeout(0.3, tester.print_message, 'is')
    timer.set_timeout(0.1, tester.print_message, 'This')
    timer.set_timeout(0.2, tester.print_message, 'sentence')
    timer.set_timeout(0.4, tester.print_message, 'synchronized')
    timer.set_timeout(0.5, tester.print_message, '.')

    time.sleep(0.6)
    try:
        assert tester.check_message().get(), "message was corrupted"
    finally:
        pykka.ActorRegistry.stop_all()
