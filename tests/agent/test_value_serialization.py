import random
import string
from datetime import datetime, timedelta
from nose.tools import assert_equals
from sys import maxint

from interpreter.value import *
from agent import value_serialization

generators = {'int': lambda: random.randint(0, maxint),
              'value_int': lambda: ValueInt(random.randint(0, maxint)),
              'float': random.random,
              'bool': lambda: random.choice([True, False]),
              'value_bool': lambda: ValueBoolean(random.choice([True, False])),
              'value_float': lambda: ValueDouble(random.random()),
              'string': lambda: ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(random.randint(1, 1000))),
              'value_string': lambda: ValueString(generators['string']()),
              'value_time': lambda: ValueTime(datetime.fromtimestamp(random.randint(0, 1000000000) + random.random())),
              'value_duration': lambda: ValueDuration(timedelta(seconds=random.randint(0, 10000) + random.random())),
              'tuple': lambda: tuple([random.choice(generators.values())() for _ in range(random.randint(1, 5))]),
              'hashable': lambda: tuple([random.choice([generators['float'], generators['int'], generators['string']])()
                                         for _ in range(random.randint(1, 5))]),
              'dict': lambda: (lambda k, v: dict(zip(k(), v())))(generators['hashable'], generators['tuple'])}

messages = [random.choice(generators.values())() for _ in range(10000)]


def test_serialization():
    out = [value_serialization.deserialize(value_serialization.serialize(message)) for message in messages]
    assert_equals(messages, out)
