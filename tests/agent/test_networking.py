import Queue
import pykka
from time import sleep
import socket
import random
import string
from sys import maxint
from nose.tools import assert_equal, with_setup
from agent.networking import Sender, Receiver
from agent.networking import DATA_SIZE, DROP_INTERVAL


class Mock_Socket(object):
    SOCKET = None

    def __init__(self):
        self.__recv = Queue.Queue()
        self.__timeout = 0

    @staticmethod
    def socket(*args):
        if Mock_Socket.SOCKET is None:
            Mock_Socket.SOCKET = Mock_Socket()
        return Mock_Socket.SOCKET

    def bind(self, *args):
        pass

    def shutdown(self, *args):
        self.__recv.join()

    def close(self, *args):
        assert self.__recv.empty(), "messages left to proceed"

    def settimeout(self, timeout):
        self.__timeout = timeout

    def recvfrom_into(self, buffer):
        try:
            message = self.__recv.get(True, self.__timeout)
            self.__recv.task_done()
            buffer[:] = message
            return len(message), (None, None)
        except Queue.Empty:
            raise socket.timeout

    def sendto(self, packet, address):
        self.__recv.put(packet)


def setup_module():
    socket.socket = Mock_Socket.socket


class SlowSender(Sender):
    def send(self, message, address):
        for packet in self._packets(message):
            sleep(1.1 * DROP_INTERVAL)
            self.socket.sendto(packet, address)


class DropCountingReceiver(Receiver):
    def __init__(self, *args, **kwargs):
        super(DropCountingReceiver, self).__init__(*args, **kwargs)
        self._drop_count = 0

    def _drop_message(self, message_id):
        super(DropCountingReceiver, self)._drop_message(message_id)
        self._drop_count += 1

    @property
    def drop_count(self):
        return self._drop_count


@with_setup(lambda: None, pykka.ActorRegistry.stop_all)
def test_receiver_drop():
    receiver = DropCountingReceiver.start().proxy()
    sender = SlowSender.start()
    sender_proxy = sender.proxy()
    MESSAGES = ['0' * 2 * DATA_SIZE]
    for message in MESSAGES:
        sender_proxy.send(message, None)
    sleep(3*DROP_INTERVAL)
    assert receiver.drop_count > 0, "messages not dropped after specified interval"
    pykka.ActorRegistry.stop_all()


@with_setup(lambda: None, pykka.ActorRegistry.stop_all)
def test_through():
    generators = {'int': lambda: random.randint(0, maxint),
                  'float': random.random,
                  'bool': lambda: random.choice([True, False]),
                  'string': lambda: ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(random.randint(1, 1000))),
                  'tuple': lambda: tuple([random.choice(generators.values())() for _ in range(random.randint(1, 3))]),
                  'hashable': lambda: tuple([random.choice([generators['float'], generators['int'], generators['string']])()
                                             for _ in range(random.randint(1, 3))]),
                  'dict': lambda: (lambda k, v: dict(zip(k(), v())))(generators['hashable'], generators['tuple'])}
    queue = Queue.Queue()
    messages = [random.choice(generators.values())() for _ in range(1000)]
    routes = [(lambda _: True, lambda _1, _2, _3, msg: queue.put(msg))]
    Receiver.start(routing=routes)
    sender = Sender.start().proxy()
    for message in messages:
        sender.send(message, None)
    for sent in messages:
        received = queue.get(True, 1)
        assert_equal(sent, received)


def teardown_module():
    reload(socket)
