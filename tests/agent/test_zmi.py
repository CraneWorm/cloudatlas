import time
import pykka
from nose.tools import assert_equals
from agent.zmi import ZMI
from agent.hierarchy import Hierarchy
from agent.timer import Timer


class Tester(pykka.ThreadingActor):
    def __init__(self, *args, **kwargs):
        super(Tester, self).__init__(*args, **kwargs)
        self._left = []
        self._right = []
        self.msg = ""

    def addChecker(self, left, right):
        self._left.append(left)
        self._right.append(right)

    def runCheck(self):
        for idx in range(len(self._left)):
            if self._left[idx] != self._right[idx]:
                self.msg += "problem with idx: %s, left: %s, right: %s" %(idx, self._left[idx], self._right[idx])
                print self.msg
                return False
        return True

def assertZMI(myZMI, compare, tester):
    tester.addChecker(myZMI, compare)

def printZMI(zmi):
    print "printZMI: %s" %zmi

def test_getterAndSetter():
    try:
        tester = Tester.start().proxy()
        timer = Timer.start().proxy()
        zmi = ZMI.start(timer=timer).proxy()
        hierarchyZMI = Hierarchy()

        violet07 = hierarchyZMI.getTreeForViolet07()
        khaki31 = hierarchyZMI.getTreeForKhaki31()

        zmi.getAll(assertZMI, None, tester)
        zmi.setAll(violet07)
        zmi.getAll(assertZMI, violet07, tester)
        zmi.setAll(khaki31)
        zmi.getAll(assertZMI, khaki31, tester)

        time.sleep(0.5)
        assert tester.runCheck().get(), "problem with test_getterAndSetter, check stdout for details"
    finally:
        pykka.ActorRegistry.stop_all()

def test_installQuery():
    try:
        timer = Timer.start().proxy()
        zmi = ZMI.start(timer=timer).proxy()
        hierarchy = Hierarchy()

        compareZMI = hierarchy.getTreeForViolet07()
        violet07 = hierarchy.getTreeForViolet07()

        zmi.setAll(violet07)
        zmi.getAll(printZMI)
        zmi.installQuery("helloAttr", ("SELECT avg(cpu_load) AS cpu_load", (long(123), None)))
        zmi.getAll(printZMI)
        zmi.uninstallQuery("helloAttr")

        time.sleep(0.5) # long enough to run all callbacks, short enough to finish before evaluateAttributes
        assert violet07 == compareZMI, "problem with test_installQuery"
    finally:
        pykka.ActorRegistry.stop_all()
