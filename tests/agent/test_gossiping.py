import time
import pykka
from nose.tools import assert_equals
from agent.gossiping import Gossiper
from agent.hierarchy import Hierarchy
from agent.zmi import ZMI
from agent.timer import Timer
from agent.networking import Sender, Receiver

from interpreter import value


def test_gossiperUpdateZMI():
    try:
        gossiper = Gossiper.start(startGossip=False).proxy()
        # remote tree should provide pjwstk ZMI instance for local tree
        hry = Hierarchy()
        localBefore = [[hry.root()], [hry.uw()], [hry.violet07()]]
        remote = [[hry.root()], [hry.pjwstk()], []]
        localAfter = [[hry.root()], [hry.uw(), hry.pjwstk()], [hry.violet07()]]
        # we define gossipLevel by structure of remote ZMI tree
        earlyTS = (10002, 10002.9)
        routeTS = (10005, 10005.8)

        offset = gossiper.getOffset(earlyTS, routeTS)
        gossiper.updateZMI(localBefore, remote, offset)
        time.sleep(3)

        assert_equals(localBefore, localAfter)
    finally:
        pykka.ActorRegistry.stop_all()

def test_fallbackContacts():
    try:
        # prepare timer
        timer = Timer.start().proxy()
        # run ZMI module and set ZMItree on violet07
        zmi = ZMI.start(timer=timer).proxy()
        hierarchyZMI = Hierarchy()
        violet07 = hierarchyZMI.getTreeForViolet07()
        for elem in violet07[-1]:
            elem.add("contacts", value.ValueSet(set()))
        for elem in violet07[-2]:
            elem.add("contacts", value.ValueSet(set()))
        zmi.setAll(violet07)
        zmi.setFallbackContacts(set([('127.0.0.1', 17311)]))
        # prepare communication and run gossiper
        sender = Sender.start().proxy()
        gossiper = Gossiper.start(timer=timer, sender=sender, zmi=zmi, startGossip=True).proxy()
        routes = [(gossiper.routeFilter, gossiper.routeReceiver)]
        Receiver.start(port=17311, routing=routes, timer=timer)

        time.sleep(2.5)

    except Exception as e:
        print e
    finally:
        pykka.ActorRegistry.stop_all()

def test_startGossip():
    try:
        # prepare timer
        timer = Timer.start().proxy()
        # run ZMI module and set ZMItree on violet07
        zmi = ZMI.start(timer=timer).proxy()
        hierarchyZMI = Hierarchy()
        violet07 = hierarchyZMI.getTreeForViolet07()
        for elem in violet07[-1]:
            elem.add("contacts", value.ValueSet(set([('127.0.0.1', 17331)])))
        for elem in violet07[-2]:
            elem.add("contacts", value.ValueSet(set([('127.0.0.1', 17331)])))
        zmi.setAll(violet07)
        # prepare communication and run gossiper
        sender = Sender.start().proxy()
        gossiper = Gossiper.start(timer=timer, sender=sender, zmi=zmi, startGossip=True).proxy()
        routes = [(gossiper.routeFilter, gossiper.routeReceiver)]
        Receiver.start(routing=routes, timer=timer)

        time.sleep(2)

    except Exception as e:
        print e
    finally:
        pykka.ActorRegistry.stop_all()
