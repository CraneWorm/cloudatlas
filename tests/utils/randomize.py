import random

from utils import randomize

def test_RoundRobin():
    try:
        n = random.randint(5, 10)
        k = random.randint(2, 5)
        e = random.randint(2, 3)
        rr = randomize.RoundRobin(n)
        rre = randomize.RoundRobinExponential(n, e)
        resRR = []
        resRRE = []
        for elem in range(k*(n-1)):
            resRR.append(rr.getValue())
            resRRE.append(rre.getValue())
        # print n, k, e
        # print resRR
        # print resRRE
        sortRR = sorted(resRR)
        sortRRE = sorted(resRRE)
        cmpArr = [x for x in range(1, n) for _ in range(k)]
    except Exception as e:
        print "exception in test: %s" %e
    assert sortRR == cmpArr, "RoundRobin problem"
    assert sortRRE == cmpArr, "RoundRobinExponential problem"

def test_RandomSelection():
    try:
        n = random.randint(5, 10)
        k = random.randint(2, 5)
        e = random.randint(2, 3)
        rs = randomize.RandomSelect(n)
        rse = randomize.RandomSelectExponential(n, e)
        resRS = []
        resRSE = []
        for elem in range(k*n):
            resRS.append(rs.getValue())
            resRSE.append(rse.getValue())
        # print n, k, e
        # print resRS
        # print resRSE
    except Exception as e:
        print "exception in test: %s" %e