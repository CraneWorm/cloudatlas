import random
from datetime import datetime, timedelta
import pykka

from interpreter.parser.parser import Parser
from interpreter.parser.lexer import Lexer
from interpreter.queryInterpreter import QueryInterpreter
from interpreter import value
from auth.querysigner import verify_query

from utils import log
from utils.config import Config

config = Config(__file__, 'agent.ini')
EVALUATE_ATTRIBUTES_TIMEOUT = config.getfloat('zmi', 'evaluate attributes timeout')
CHECK_ATTRIBUTES_TIMEOUT = config.getfloat('zmi', 'check attributes timeout')
REMOVE_THRESHOLD = config.getfloat('zmi', 'remove threshold')
CONTACTS_SET_MAX_SIZE = 3

DEBUG_NO_QUERY_VERIFY = False

loggerInfo = log.getLogger('', 'zmiInfo')
logger = log.getLogger('agent', __name__)
if DEBUG_NO_QUERY_VERIFY:
    logger.warning("debug option DEBUG_NO_QUERY_VERIFY is ON")

class UriError(Exception):
    pass

class ZMI(pykka.ThreadingActor):

    def __init__(self, timer=None):
        super(ZMI, self).__init__()
        self._timer = timer
        self._zmi = None
        self._fallback = set()
        self._parser = Parser(outputdir='tests/interpreter/parser')
        self._lexer = Lexer(debug=0)
        self._interpreter = QueryInterpreter()
        self._installedQueries = {}

    def on_start(self):
        logger.info("on start")
        self.timeoutLoggerInfo()
        self.timeoutEvaluateAttributes()
        self.timeoutCheckAttributes()

    def on_stop(self):
        logger.info("on stop")


    def installQuery(self, queryName, queryInstance):
        try:
            logger.info("installQuery: %s" %queryName)
            queryString, (sig_str, _) = queryInstance
            sig = (long(sig_str), None)
            if verify_query(queryString, sig) or DEBUG_NO_QUERY_VERIFY:
                self._lexer.input(queryString)
                query = self._parser.parse(lexer=self._lexer, debug=0)
                queryStatus = 1 # installed
                queryTimestamp = datetime.now()
                for level in range(len(self._zmi)-1-1, -1, -1): # from level one up leaf zone to root
                    logger.info("installing query %s on level: %s" %(queryName, level))
                    mainNode = self._zmi[level][0]
                    childrenNodes = self._zmi[level+1]
                    queryResult = self._interpreter(mainNode, childrenNodes, (queryName, queryString), query)
                    logger.info("installed query %s on level: %s" %(queryName, level))
                    # TODO: the latest instance is with queryResult for root installation
                    # TODO: maybe instead of queryResult use attrs list from parser
                    self._installedQueries[queryName] = (queryInstance, queryResult, queryStatus, queryTimestamp)
                return True
            logger.warning("query %s cannot be verified" %queryName)
            return False
        except Exception as e:
            import traceback
            traceback.print_exc()
            logger.error("exception in installQuery: %s" %e)
            raise e

    def uninstallQuery(self, queryName):

        def removeAttrFromOwnZones(attr):
            for level in range(len(self._zmi)-1): # from root to level one up leaf zone
                self._zmi[level][0].remove(attr)

        try:
            logger.info("uninstallQuery: %s" %queryName)
            if queryName in self._installedQueries:
                queryInstance, queryResultList, queryStatus, _ = self._installedQueries[queryName]
                if queryStatus:
                    queryStatus = 0 # uninstalled
                    queryTimestamp = datetime.now()
                    removeAttrFromOwnZones(queryName)
                    for results in queryResultList: # for each Query
                        for resVal, resAttr in results: # for each result attribute
                            removeAttrFromOwnZones(resAttr)
                    self._installedQueries[queryName] = (queryInstance, queryResultList, queryStatus, queryTimestamp)
                else:
                    logger.warning("query already uninstalled, status: %s" %queryStatus)
            else:
                logger.warning("cannot find instance of query %s in _installedQueries" %queryName)
        except Exception as e:
            import traceback
            traceback.print_exc()
            logger.error("exception in uninstallQuery: %s" %e)
            raise e

    def runLoggerInfo(self):
        def getTreeOfAttributes(attributes):
            if self._zmi is None:
                return None
            return [[[(attribute, elem.get(attribute)) for attribute in attributes] for elem in levelList] for levelList in self._zmi]

        def printTree(tree):
            if tree is None:
                return "[empty zmi]"
            res = ""
            for levelList in tree:
                for attrList in levelList:
                    res += "\n"
                    for key, val in attrList:
                        res += "\t%s: %s" %(key, val)
            return res

        def getQueriesNames(queryDict):
            return [key for key in queryDict]

        # loggerInfo.info("zmi tree after evaluation of attributes: %s" %self._zmi)
        loggerInfo.info("-------------------------------------- zmi info --------------------------------------------")
        loggerInfo.info("zmi tree: %s" %printTree(getTreeOfAttributes(["name", "cpu_load", "num_processes", "free_ram", "q1"])))
        loggerInfo.info("installedQueries: %s" %getQueriesNames(self._installedQueries))

    def timeoutLoggerInfo(self):
        self.runLoggerInfo()
        self._timer.set_timeout(EVALUATE_ATTRIBUTES_TIMEOUT, self.timeoutLoggerInfo)

    def timeoutEvaluateAttributes(self):
        self._timer.set_timeout(EVALUATE_ATTRIBUTES_TIMEOUT, self.evaluateAttributes)

    def timeoutCheckAttributes(self):
        self._timer.set_timeout(CHECK_ATTRIBUTES_TIMEOUT, self.checkAttributes)

    def installQueryDict(self, queryDict):
        logger.info("installQueryDict")
        for queryName in queryDict:
            queryInstance, _, queryStatus, _ = queryDict[queryName]
            if queryStatus:
                self.installQuery(queryName, queryInstance)

    def uninstallQueryDict(self, queryDict):
        logger.info("uninstallQueryDict")
        for queryName in queryDict:
            _, _, queryStatus, _ = queryDict[queryName]
            if not queryStatus:
                self.uninstallQuery(queryName)

    def evaluateAttributes(self):
        logger.info("evaluateAttributes")
        self.timeoutEvaluateAttributes()
        # start evaluation of queries
        self.installQueryDict(self._installedQueries)
        # start evaluation of attributes
        for level in range(len(self._zmi)-1-1, -1, -1): # from one above leaf to root
            mainNode = self._zmi[level][0]
            childrenNodes = self._zmi[level+1]
            contactsSet = set()
            for child in childrenNodes:
                for contact in child.get("contacts").val:
                    contactsSet.add(contact)
            if len(contactsSet) > CONTACTS_SET_MAX_SIZE:
                contactsSet = set(random.sample(contactsSet, CONTACTS_SET_MAX_SIZE))
            mainNode.add("contacts", value.ValueSet(contactsSet))
            mainNode.add("timestamp", value.ValueTime(datetime.now()))

    def checkAttributes(self):
        logger.info("checkAttributes")
        self.timeoutCheckAttributes()
        # start checker
        tmpTree = []
        for i in range(len(self._zmi)): # from the root to the leaf
            tmpTree.append([])
            for j in range(len(self._zmi[i])):
                if j == 0: # always keep own zones
                    tmpTree[i].append(self._zmi[i][j])
                    continue
                zmiTimestamp = self._zmi[i][j].get("timestamp").val
                nowTimestamp = datetime.now()
                if zmiTimestamp+timedelta(seconds=REMOVE_THRESHOLD) >= nowTimestamp: # keep this one
                    tmpTree[i].append(self._zmi[i][j])
                else:
                    logger.info("removing too old attribute: %s, on level: %s" %(self._zmi[i][j].get("name"), i))
        self._zmi = tmpTree

    def createZMI(self, myLevel):
        self._zmi = [[] for _ in range(myLevel+1)]

    def eraseZMI(self):
        self._zmi = None

    def setAll(self, wholeZMI):
        self._zmi = wholeZMI
        self.runLoggerInfo()

    def getAll(self, callback, *args):
        if self._zmi is not None:
            callback(self._zmi, *args)
        else:
            logger.warning("getAll found _zmi empty - no callback")

    def setQueries(self, wholeQueryDict):
        self._installedQueries = wholeQueryDict

    def getTreeAndQueries(self, callback, *args):
        if self._zmi is not None:
            callback(self._zmi, self._installedQueries, *args)
        else:
            logger.warning("getTreeAndQueries found _zmi empty - no callback")

    def setFallbackContacts(self, fallbackContacts):
        self._fallback = fallbackContacts

    def getFallbackContacts(self, callback, *args):
        if self._fallback:
            callback(self._fallback, *args)
        else:
            logger.warning("getFallbackContacts found _fallback empty - no callback")

    def setAttributesForLevel(self, level, listOfMapsOfAttributes):
        self._zmi[level] = listOfMapsOfAttributes

    def getAttributesForLevel(self, level, callback, *args):
        callback(self._zmi[level], *args)

    def setAttributes(self, level, sibling, mapOfAttributes):
        self._zmi[level][sibling] = mapOfAttributes

    def getAttributes(self, level, sibling, callback, *args):
        callback(self._zmi[level][sibling], *args)

    def feedAttributes(self, attributes):
        attributes['timestamp'] = value.ValueTime(datetime.now())
        self._zmi[-1][0]._dict.update(attributes)
        return True

    def getZmiByURI(self, uri):
        try:
            if uri:
                for name, zmis in zip(uri[:-1], self._zmi[1:]):
                    assert(name == zmis[0].get('name').val)
                return next(zmi for zmi in self._zmi[len(uri)] if zmi.get('name').val == uri[-1])
            else:
                return self._zmi[0][0]
        except (AssertionError, IndexError, StopIteration):
            raise UriError(uri)

    def getZmiNames(self):
        return [[zmi.get('name') for zmi in zmis] for zmis in self._zmi]
#
# ZMI_START = ZMI().start()
# ZMI_PROXY = ZMI_START.proxy()
#
#
# def getZMI(callback, *args):
#     return ZMI_PROXY.getAll(callback, *args)
#
# def setZMI(zmi):
#     return ZMI_PROXY.setAll(zmi)
#
# __all__ = [getZMI, setZMI]
