import msgpack
import itertools
import datetime
from collections import defaultdict

from interpreter import value
from interpreter import attributesMap

VALUE_TO_CODE = {v: k for v, k in zip([value.ValueInt, value.ValueDouble, value.ValueBoolean,
                                       value.ValueString, value.ValueTime, value.ValueDuration,
                                       value.ValueNone, value.ValueContact, value.ValueSet, value.ValueQuery,
                                       value.ValueList, set, datetime.datetime, attributesMap.AttributesMap, value.OneResult], itertools.count())}

CODE_TO_VALUE = {k: v for v, k in VALUE_TO_CODE.iteritems()}
to_code = lambda cls: VALUE_TO_CODE[cls]
VALUE_SERIALIZER = defaultdict(lambda: lambda obj: obj.val)
VALUE_SERIALIZER.update({
    value.ValueTime: lambda obj: (obj.val.year, obj.val.month, obj.val.day,
                                  obj.val.hour, obj.val.minute, obj.val.second,
                                  obj.val.microsecond, obj.val.tzinfo),
    value.ValueDuration: lambda obj: obj.val.total_seconds(),
    datetime.datetime: lambda obj: (obj.year, obj.month, obj.day,
                                    obj.hour, obj.minute, obj.second,
                                    obj.microsecond, obj.tzinfo),
    set: list,
    attributesMap.AttributesMap: lambda obj: obj._dict})
VALUE_DESERIALIZER = defaultdict(lambda: lambda code, data: CODE_TO_VALUE[code](data))
VALUE_DESERIALIZER.update({
    to_code(datetime.datetime): lambda code, data: datetime.datetime(*data),
    to_code(value.ValueTime): lambda code, data: value.ValueTime(datetime.datetime(*data)),
    to_code(value.ValueDuration): lambda code, data: value.ValueDuration(datetime.timedelta(seconds=data)),
    to_code(set): lambda code, data: set(data)})
value_to_bytes = lambda obj: msgpack.packb(VALUE_SERIALIZER[obj.__class__](obj), use_bin_type=True, default=packb_value)
bytes_to_value = lambda code, data: VALUE_DESERIALIZER[code](code, msgpack.unpackb(data, use_list=False, ext_hook=unpackb_value))


def packb_value(obj):
    if isinstance(obj, (value.Value, set, list, attributesMap.AttributesMap, value.OneResult, datetime.datetime)):
        if value.isNone(obj):
            return msgpack.ExtType(to_code(value.ValueNone), msgpack.packb(None))
        return msgpack.ExtType(to_code(obj.__class__), value_to_bytes(obj))
    return msgpack.packb(obj, use_bin_type=True)


def unpackb_value(code, data):
    if code in CODE_TO_VALUE.keys():
        if code == to_code(value.ValueNone):
            return value.ValueNone()
        return bytes_to_value(code, data)
    return msgpack.ExtType(code, data)


def serialize(message):
    return msgpack.packb(message, use_bin_type=True, default=packb_value)


def deserialize(message):
    return msgpack.unpackb(message, encoding='utf8', use_list=False, ext_hook=unpackb_value)
