#!/usr/bin/python

import pykka
import struct
import threading
import socket
import time
import random
from collections import namedtuple
from sys import maxint

import value_serialization
from timer import Timer
from utils.config import Config

config = Config(__file__, 'agent.ini')

DATA_SIZE = 1024
MESSAGE_FORMAT = '! d Q i i i %ds' % DATA_SIZE
BUFFER_SIZE = struct.Struct(MESSAGE_FORMAT).size
RECV_PORT = config.getint('networking', 'port')
SOCKET_POLL_TIMEOUT = config.getfloat('networking', 'socket poll timeout')
DROP_INTERVAL = config.getfloat('networking', 'drop interval')

Packet = namedtuple('Packet', ['timestamp', 'message_id', 'chunk_number', 'chunks_count', 'data_length', 'chunk_data'])


class Sender(pykka.ThreadingActor):
    def on_start(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def on_stop(self):
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

    def _chunks(self, message):
        packed_data = value_serialization.serialize(message)
        return [ch for ch in enumerate([packed_data[i:i+DATA_SIZE] for i in range(0, len(packed_data), DATA_SIZE)])]

    def _packets(self, message):
        timestamp = time.time()
        message_id = random.randint(0, maxint)
        packet_struct = struct.Struct(MESSAGE_FORMAT)
        chunks = self._chunks(message)
        chunks_count = len(chunks)
        for chunk_number, chunk_data in chunks:
            yield packet_struct.pack(timestamp, message_id, chunk_number, chunks_count, len(chunk_data), chunk_data)

    def send(self, message, address):
        for packet in self._packets(message):
            self.socket.sendto(packet, address)


class Receiver(pykka.ThreadingActor):
    def __init__(self, port=RECV_PORT, interface="", routing=None, timer=None):
        super(Receiver, self).__init__()
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__socket.settimeout(SOCKET_POLL_TIMEOUT)
        self.__socket.bind((interface, port))
        self.__receiver = threading.Thread(target=self._receiver_thread)
        self.__buffer = bytearray(BUFFER_SIZE)
        self.__inbox = {}
        self.__routing = routing if routing is not None else []
        self.__running = True
        if timer is None:
            self.__timer = Timer.start().proxy()
        else:
            self.__timer = timer
        self.__receiver.start()

    def on_stop(self):
        self.__running = False
        self.__socket.shutdown(socket.SHUT_RDWR)
        self.__socket.close()
        self.__receiver.join()
        self.__timer.stop()

    def _drop_message(self, message_id):
        if message_id in self.__inbox:
            self.__inbox.pop(message_id)

    def _receiver_thread(self):
        packet_struct = struct.Struct(MESSAGE_FORMAT)
        while self.__running:
            try:
                cnt, addr = self.__socket.recvfrom_into(self.__buffer)
                if cnt:
                    data = Packet._make(packet_struct.unpack(self.__buffer))
                    message_id = data.message_id
                    if message_id not in self.__inbox:
                        self.__inbox[message_id] = {'data': bytearray(data.chunks_count * DATA_SIZE),
                                                    'missing': set(range(data.chunks_count))}
                        self.__timer.set_timeout(DROP_INTERVAL, self._drop_message, message_id)
                    data_offset = data.chunk_number * DATA_SIZE
                    self.__inbox[message_id]['data'][data_offset: data_offset + DATA_SIZE] = data.chunk_data[:data.data_length]
                    self.__inbox[message_id]['missing'].discard(data.chunk_number)
                    if not len(self.__inbox[message_id]['missing']):
                        message = value_serialization.deserialize(self.__inbox[message_id]['data'])
                        timestamp_out = data.timestamp
                        timestamp_in = time.time()
                        for (route, callback) in self.__routing:
                            if route(message):
                                callback(addr, timestamp_out, timestamp_in, message)
                        self.__inbox.pop(message_id)
            except socket.timeout:
                # print "hearbeat"
                pass

#RECEIVER = Receiver().start()
#SENDER = Sender().start()
#SENDER_PROXY = SENDER.proxy()


#def send(message, address):
#    return SENDER_PROXY.send(message, address)

#__all__ = [send]
