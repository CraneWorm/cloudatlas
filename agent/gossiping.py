import pykka
import time
import random
from sys import maxint
from datetime import timedelta

from utils import log
from utils.config import Config
from utils.randomize import RoundRobin, RandomSelectExponential, RandomSelect, RoundRobinExponential

config = Config(__file__, 'agent.ini')
SESSION_TIMEOUT = config.getint('gossiping', 'session timeout')
GOSSIPING_TIMEOUT = config.getfloat('gossiping', 'gossiping timeout')
GOSSIPING_ATTEMPTS = config.getint('gossiping', 'gossiping attempts')
RANDOMIZE_FUNCTION = config.get('gossiping', 'randomize function')
LEVEL_DRAWS = 3

DEBUG_GOSSIP_YOURSELF_ALLOWED = False

logger = log.getLogger('agent', __name__)
if DEBUG_GOSSIP_YOURSELF_ALLOWED:
    logger.warning("debug option DEBUG_GOSSIP_YOURSELF_ALLOWED is ON")

class Gossiper(pykka.ThreadingActor):

    ###     INIT CODE

    def __init__(self, port=17331, timer=None, sender=None, zmi=None, startGossip=True):
        super(Gossiper, self).__init__()
        self._timer = timer
        self._sender = sender
        self._zmi = zmi
        self._startGossip = startGossip
        self._port = port
        self._session = {}
        self._routeDict = {
            "requestGossip":    self.requestGossip,
            "receiveGossip":    self.receiveGossip,
            "responseGossip":   self.responseGossip
        }
        self._randomizeDict = {
            "RoundRobin":               RoundRobin,
            "RoundRobinExponential":    RoundRobinExponential,
            "RandomSelect":             RandomSelect,
            "RandomSelectExponential":  RandomSelectExponential
        }
        self._randomize = None

    def on_start(self):
        logger.info("on start")
        if self._startGossip:
            self.timeoutGossip()

    def on_stop(self):
        logger.info("on stop")


    ###     SESSION CODE

    def startSession(self, *args):
        sessionId = random.randint(0, maxint)
        self._session[sessionId] = args+(sessionId,)
        self.send(*self._session[sessionId])
        self._timer.set_timeout(SESSION_TIMEOUT, self.attemptStopSession, sessionId, GOSSIPING_ATTEMPTS)

    def checkSession(self, sessionId):
        if sessionId in self._session:
            return True
        return False

    def attemptStopSession(self, sessionId, attemptCounter):
        if sessionId in self._session:
            attemptCounter -= 1
            if attemptCounter > 0:
                logger.info("attemptCounter: %s" %attemptCounter)
                self.send(*self._session[sessionId])
                self._timer.set_timeout(SESSION_TIMEOUT, self.attemptStopSession, sessionId, attemptCounter)
            else:
                self.stopSession(sessionId)

    def stopSession(self, sessionId):
        if sessionId in self._session:
            del self._session[sessionId]


    ###     ROUTE CODE

    def routeFilter(self, message):
        state, args = message
        if state in self._routeDict:
            return True
        return False

    def routeReceiver(self, remoteAddr, remoteTS, localTS, message):
        if remoteAddr[0] is None or remoteAddr[1] is None:
            logger.warning("remoteAddr is None: %s, %s" %remoteAddr)
            return
        state, args = message
        logger.info("routeReceiver got state: %s from remoteAddr: %s" %(state, remoteAddr))
        self._routeDict[state](remoteAddr, (remoteTS, localTS), *args)

    ###     UTIL FUNCTIONS CODE

    def send(self, addr, state, *args):
        logger.info("gossiping send function, state: %s, addr: %s" %(state, addr))
        message = (state, args)
        self._sender.send(message, addr)

    def timeoutGossip(self):
        self._timer.set_timeout(GOSSIPING_TIMEOUT, self.startGossip)

    def initRandomize(self, size):
        if self._randomize is None:
            if RANDOMIZE_FUNCTION in self._randomizeDict:
                self._randomize = self._randomizeDict[RANDOMIZE_FUNCTION](size)
            else:
                logger.error("RANDOMIZE_FUNCTION not found: %s" %RANDOMIZE_FUNCTION)
                raise Exception("RANDOMIZE_FUNCTION not found: %s" %RANDOMIZE_FUNCTION)

    def cutTreeZMI(self, tree, nameList):

        def findGossipLevel(tree, nameList):
            for level in range(1, len(tree)): # from one below root to leaf
                if tree[level][0].get("name").val != nameList[level]:
                    return level
            return len(tree)-1 # leaf level

        try:
            allowedLevel = findGossipLevel(tree, nameList)
            resTree = []
            for i in range(len(tree)):
                if i <= allowedLevel:
                    resTree.append([])
                    for j in range(len(tree[i])):
                        if tree[i][j].get("name").val not in nameList:
                            resTree[i].append(tree[i][j])
                else:
                    resTree.append([])
            return resTree
        except Exception as e:
            logger.error("exception cutTreeZMI: %s" %e)
            raise e

    def _getNameList(self, myZMI):
        nameList = []
        for elem in myZMI: # from root to leaf
            if elem:
                nameList.append(elem[0].get("name").val)
            else:
                nameList.append(None)
        return nameList


    ###     STATE FUNCTIONS CODE

    def startGossip(self):

        def sendState(remoteAddr, nameList):
            # session will provide k attempts if needed
            logger.info("start gossiping with remoteAddr: %s" %(remoteAddr,))
            self.startSession(remoteAddr, "requestGossip", nameList, self._port)

        def fallbackContactsCallback(contactSet, nameList):
            logger.info("fallbackContactsCallback: %s", contactSet)
            contactList = list(contactSet)
            random.shuffle(contactList)
            sendState(contactList[0], nameList)

        def findContact(myZMI):
            for lookingCounter in range(LEVEL_DRAWS):
                level = self._randomize.getValue()
                logger.info("lookingCounter: %s, looking for contacts on level: %s" %(lookingCounter, level))
                brothers = myZMI[level][1:]
                if not brothers:
                    logger.info("empty brothers list on level: %s" %level)
                    continue
                random.shuffle(brothers)
                for brother in brothers:
                    logger.info("consider brother: %s" %brother.get("name"))
                    contacts = list(brother.get("contacts"))
                    if not contacts:
                        logger.info("brother: %s has empty contact list" %brother.get("name"))
                        continue
                    random.shuffle(contacts)
                    return contacts[0], level
            return None, None

        def zmiCallback(myZMI):
            logger.info("zmiCallback of startGossip")
            self.initRandomize(len(myZMI)) # something that should happen in init but after zmi setup, so len(ZMI) is known
            nameList = self._getNameList(myZMI)
            contact, level = findContact(myZMI)
            if contact is None:
                logger.info("no contacts found on all drew levels in all brothers - try fallback contacts")
                self._zmi.getFallbackContacts(fallbackContactsCallback, nameList)
                return
            logger.info("found contact: %s, on level: %s" %((contact,), level))
            sendState(contact, nameList)


        logger.info("startGossip")
        # another timeout for future gossiping
        self.timeoutGossip()
        # asynchronous ZMI getter
        self._zmi.getAll(zmiCallback)


    def requestGossip(self, remoteAddr, routeTS, remoteNameList, remotePort, sessionId):

        def zmiCallback(myZMI, myQueries):
            logger.info("zmiCallback of requestGossip")
            nameList = self._getNameList(myZMI)
            # send state "receiveGossip" to the remoteAddr agent
            remoteIP, _ = remoteAddr
            self.send((remoteIP, remotePort), "receiveGossip", routeTS, self.cutTreeZMI(myZMI, remoteNameList), myQueries, nameList, self._port, sessionId)

        logger.info("requestGossip")
        if not DEBUG_GOSSIP_YOURSELF_ALLOWED:
            if self.checkSession(sessionId):
                logger.warning("gossiping request from itself")
                self.stopSession(sessionId)
                return
        self._zmi.getTreeAndQueries(zmiCallback)


    def receiveGossip(self, remoteAddr, routeTS, earlyTS, remoteZMI, remoteQueries, remoteNameList, remotePort, sessionId):

        def zmiCallback(myZMI, myQueries):
            logger.info("zmiCallback of receiveGossip")
            remoteIP, _ = remoteAddr
            # update local ZMI database and send response with own ZMI tree
            self.send((remoteIP, remotePort), "responseGossip", routeTS, self.cutTreeZMI(myZMI, remoteNameList), myQueries)
            offset = self.getOffset(earlyTS, routeTS)
            self.updateZMI(myZMI, remoteZMI, offset)
            self.updateQueries(myQueries, remoteQueries, offset)

        logger.info("receiveGossip")
        if self.checkSession(sessionId):
            self.stopSession(sessionId)
            self._zmi.getTreeAndQueries(zmiCallback)


    def responseGossip(self, remoteAddr, routeTS, earlyTS, remoteZMI, remoteQueries):

        def zmiCallback(myZMI, myQueries):
            logger.info("zmiCallback of responseGossip")
            # just update ZMI database
            offset = self.getOffset(earlyTS, routeTS)
            self.updateZMI(myZMI, remoteZMI, offset)
            self.updateQueries(myQueries, remoteQueries, offset)

        logger.info("responseGossip")
        self._zmi.getTreeAndQueries(zmiCallback)


    ###     UPDATE FUNCTION CODE

    def getOffset(self, earlyTS, routeTS):
        a1, b1 = earlyTS
        b2, a2 = routeTS
        roundTripDelay = (a2-a1) - (b2-b1)
        offset = b2 + roundTripDelay/2 - a2
        return timedelta(seconds=offset)


    def updateQueries(self, myQueries, remoteQueries, offset):

        def updateTimestamp(singleQuery, rOffset):
            queryInstance, queryResultList, queryStatus, queryTimestamp = singleQuery
            return queryInstance, queryResultList, queryStatus, queryTimestamp+rOffset

        logger.info("updateQueries")
        logger.debug("myQueries: %s" %myQueries)
        logger.debug("remoteQueries: %s" %remoteQueries)
        queriesToInstall = {}
        queriesToUninstall = {}
        for queryName in remoteQueries:
            _, _, remoteStatus, remoteTimestamp = remoteQueries[queryName]
            if queryName not in myQueries: # completely new query
                logger.info("query %s is new for us" %queryName)
                myQueries[queryName] = remoteQueries[queryName]
                if remoteStatus:
                    queriesToInstall[queryName] = myQueries[queryName]
            else: # query already exists
                logger.info("query %s from remote already exist" %queryName)
                # TODO: so maybe we should do nothing (if names are unique)
                _, _, _, myTimestamp = myQueries[queryName]
                if myTimestamp+offset < remoteTimestamp: # we have older instance
                    logger.info("update fresher remote query: %s, using offset: %s" %(queryName, offset))
                    myQueries[queryName] = updateTimestamp(remoteQueries[queryName], -offset)
                    if remoteStatus:
                        queriesToInstall[queryName] = myQueries[queryName]
                    else:
                        queriesToUninstall[queryName] = myQueries[queryName]
        logger.info("queriesToInstall: %s" %queriesToInstall)
        logger.info("queriesToUninstall: %s" %queriesToUninstall)
        self._zmi.setQueries(myQueries)
        self._zmi.installQueryDict(queriesToInstall)
        self._zmi.uninstallQueryDict(queriesToUninstall)


    def updateZMI(self, myZMI, remoteZMI, offset):

        def findIndexByName(elemZMI, listZMI):
            # returns found element's index from listZMI or None if not found
            name = elemZMI.get("name").val
            for idx, elem in enumerate(listZMI):
                if elem.get("name").val == name:
                    return idx
            return None

        def shouldBeUpdated(localZMI, remoteZMI, offset):
            # returns True if localZMI has earlier timestamp
            local = localZMI.get("timestamp")
            remote = remoteZMI.get("timestamp")
            # print "local ts: %s, remote ts: %s, offset: %s" %(local.val+offset, remote.val, offset)
            return local.val+offset < remote.val

        def updateTimestamp(singleZMI, rOffset):
            ts = singleZMI.get("timestamp")
            ts.val += rOffset
            singleZMI.add("timestamp", ts)
            return singleZMI

        logger.info("updateZMI")
        for level in range(len(remoteZMI)-1, 0, -1): # all the way down to one before root level which is 0
            listOfRemoteZMI = remoteZMI[level]
            logger.info("update on level %s, length of listOfRemoteZMI: %s" %(level, len(listOfRemoteZMI)))
            for singleRemoteZMI in listOfRemoteZMI:
                logger.info("consider remote zmi: %s" %singleRemoteZMI.get("name"))
                foundLocalIndex = findIndexByName(singleRemoteZMI, myZMI[level])
                if foundLocalIndex is not None:
                    if foundLocalIndex == 0: # found as own local zone
                        # should be never executed, because remoteZMI should not contain ZMIs from remoteNameList zones
                        logger.warning("found local but it's our zone - interrupt")
                        logger.error("should be never True: foundLocalIndex == 0")
                        continue
                    foundLocalZMI = myZMI[level][foundLocalIndex]
                    logger.info("found local ZMI: %s" %foundLocalZMI.get("name"))
                    if shouldBeUpdated(foundLocalZMI, singleRemoteZMI, offset):
                        logger.info("should be updated, using offset %s" %offset)
                        myZMI[level][foundLocalIndex] = updateTimestamp(singleRemoteZMI, -offset)
                else:
                    logger.info("added to local tree considered remote: %s" %singleRemoteZMI.get("name"))
                    myZMI[level].append(singleRemoteZMI)
        self._zmi.setAll(myZMI)
