import rpyc
import pykka
import threading
from rpyc.utils.server import ThreadPoolServer


from utils.config import Config
from value_serialization import deserialize, serialize

config = Config(__file__, 'agent.ini')
FEEDER_PORT = config.getint('feeder service', 'port')


class FeederService(rpyc.Service):
    def exposed_feed_attributes(self, attributes):
        attrs = deserialize(attributes)
        return self._zmi.feedAttributes(attrs).get()

    def exposed_get_zmi_for_uri(self, uri_str=None):
        uri = [] if uri_str is None else uri_str.split('/')
        return serialize(self._zmi.getZmiByURI(uri).get())

    def exposed_get_zmi_names(self):
        return serialize(self._zmi.getZmiNames().get())

    def exposed_set_fallback_contacts(self, contacts):
        self._zmi.setFallbackContacts(deserialize(contacts))

    def exposed_install_query(self, name, data):
        query_str, (sig_str, _) = deserialize(data)
        query = (query_str, (sig_str, None))
        return self._zmi.installQuery(name, query).get()

    def exposed_list_queries(self):
        pass

class Feeder(pykka.ThreadingActor):
    def __init__(self, zmi, port=FEEDER_PORT,  *args, **kwargs):
        super(Feeder, self).__init__()
        FeederService._zmi = zmi
        self._server = ThreadPoolServer(service=FeederService, port=port)
        self.service = threading.Thread(target=self._server.start)

    def on_start(self):
        self.service.start()

    def on_stop(self):
        self._server.close()
        self.service.join()
