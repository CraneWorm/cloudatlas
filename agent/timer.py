import pykka
import threading
import heapq
import Queue
from datetime import datetime, timedelta


class Timer(pykka.ThreadingActor):
    def __init__(self):
        super(Timer, self).__init__()
        self.timeouts = []
        self.requests = Queue.Queue()

    def on_start(self):
        self.__running = True
        self.dispatcher = threading.Thread(target=self._dispatcher_thread)
        self.dispatcher.start()

    def on_stop(self):
        self.__running = False
        self.requests.put((datetime.now(), None, None))
        self.dispatcher.join()

    def set_timeout(self, seconds, callback, *args):
        delta = timedelta(seconds=seconds)
        self.requests.put((datetime.now() + delta, callback, args))

    def _dispatcher_thread(self):
        while self.__running:
            if self.timeouts:  # there are pending timeout requests
                (timeout, _, _) = self.timeouts[0]
            else:
                timeout = None
            try:
                # wait for another request or until the first scheduled timeout
                seconds = max(0, (timeout - datetime.now()).total_seconds()) if timeout else None
                request = self.requests.get(True, seconds)
                heapq.heappush(self.timeouts, request)
            except Queue.Empty:
                # whe hit a scheduled timeout, dispatch
                (timeout, callback, args) = heapq.heappop(self.timeouts)
                callback(*args)

#TIMER = Timer.start()
#TIMER_PROXY = TIMER.proxy()
#
#
#def set_timeout(seconds, callable, *args):
#    return TIMER_PROXY.set_timeout(seconds, callable, *args)
#
#__all__ = [set_timeout]
