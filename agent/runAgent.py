import sys
import time
import datetime

import os

import pykka

_datetime = datetime.datetime
class MockDatetime(_datetime):
    def __init__(self, *args):
        super(MockDatetime, self).__init__(*args)
    @staticmethod
    def now():
        now = _datetime.now() + datetime.timedelta(seconds=int(os.getenv('OFFSET', 0)))
        return MockDatetime(now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond, now.tzinfo)
    def __getattr__(self, attr):
        return getattr(_datetime, attr)

datetime.datetime = MockDatetime

from timer import Timer
from zmi import ZMI
from hierarchy import Hierarchy
from networking import Sender, Receiver
from gossiping import Gossiper
from service import Feeder

from interpreter import value as v
from interpreter.attributesMap import *
from utils.config import Config
config = Config(__file__, 'agent.ini')

RECEIVE_IF = "127.0.0.1"

def getTestAttributesMap(name="/", level=0, timestamp=datetime.datetime.now(), contacts=None):
    attrM = AttributesMap()
    attrM.add("name", v.ValueString(name))
    attrM.add("level", v.ValueInt(level))
    attrM.add("timestamp", v.ValueTime(timestamp))
    attrM.add("contacts", v.ValueSet(contacts if contacts is not None else set()))
    attrM.add("cardinality", v.ValueInt(1))
    attrM.add("creation", v.ValueTime(datetime.datetime.now()))
    attrM.add("cpu_usage", v.ValueDouble(0.4))
    attrM.add("num_cores", v.ValueInt(13))
    return attrM

def getDictOfTrees():
    root = getTestAttributesMap(name="/", level=0, timestamp=datetime.datetime.now(), contacts=set())

    odd = getTestAttributesMap(name="odd", level=1, timestamp=datetime.datetime.now(), contacts=set([('localhost', 17331), ('localhost', 17333), ('localhost', 17335)]))
    even = getTestAttributesMap(name="even", level=1, timestamp=datetime.datetime.now(), contacts=set([('localhost', 17332), ('localhost', 17334)]))

    one = getTestAttributesMap(name="one", level=2, timestamp=datetime.datetime.now(), contacts=set([('localhost', 17331)]))
    two = getTestAttributesMap(name="two", level=2, timestamp=datetime.datetime.now(), contacts=set([('localhost', 17332)]))
    three = getTestAttributesMap(name="three", level=2, timestamp=datetime.datetime.now(), contacts=set([('localhost', 17333)]))
    four = getTestAttributesMap(name="four", level=2, timestamp=datetime.datetime.now(), contacts=set([('localhost', 17334)]))
    five = getTestAttributesMap(name="five", level=2, timestamp=datetime.datetime.now(), contacts=set([('localhost', 17335)]))

    return {
        "odd_even/one": [[root], [odd], [one]],
        "odd_even/two": [[root], [even], [two]],
        "odd_even/three": [[root], [odd], [three]],
        "odd_even/four": [[root], [even], [four]],
        "odd_even/five": [[root], [odd], [five]]
    }


def main(argv):
    # argv = [0: file, 1: instanceName, 2: port, 3: seconds]
    try:
        forest = getDictOfTrees()
        if len(argv) != 2:
            raise Exception("Wrong number of arguments")
        instanceName = os.getenv('ATLAS_CONFIG', "one")
        if instanceName not in forest:
            raise Exception("Wrong instance: %s" %instanceName)
        port = config.getint('networking', 'port')
        print port
        seconds = int(argv[1])

        timer = Timer.start().proxy()
        zmi = ZMI.start(timer=timer).proxy()
        zmi.setAll(forest[instanceName])
        sender = Sender.start().proxy()
        gossiper = Gossiper.start(port=port, timer=timer, sender=sender, zmi=zmi).proxy()
        routes = [(gossiper.routeFilter, gossiper.routeReceiver)]
        Receiver.start(port=port, interface=RECEIVE_IF, routing=routes, timer=timer)
        Feeder.start(zmi=zmi).proxy()

        time.sleep(seconds)
    except Exception as e:
        import traceback
        traceback.print_exc()
        print "exception: %s" %e
    finally:
        pykka.ActorRegistry.stop_all()

if __name__ == "__main__":
    main(sys.argv)
