import os
from ConfigParser import SafeConfigParser


class Config(object):
    def __init__(self, module, filename):
        self._config = SafeConfigParser()
        application_config = os.getenv('ATLAS_CONFIG', 'localhost')
        config_root = os.path.abspath(os.path.join(os.path.realpath(__file__), '../../config/%s' % application_config))
        path = os.path.join(config_root, filename)
        if not self._config.read(path):
            raise IOError("Unable to read configuration file!")

    def __getattr__(self, method):
        return getattr(self._config, method)
