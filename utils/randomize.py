import random

class RoundRobin(object):
    def __init__(self, possibilities):
        self._possibilities = possibilities
        self._fillSet()

    def _fillSet(self):
        self._set = set(range(1, self._possibilities))

    def getValue(self):
        if not self._set:
            self._fillSet()
        samples = random.sample(self._set, 1)
        self._set.remove(samples[0])
        return samples[0]

class RoundRobinExponential(object):
    def __init__(self, possibilities, exponent=2):
        self._possibilities = possibilities
        self._exponent = exponent
        self._fillSet()

    def _fillSet(self):
        self._set = set(zip(range(self._possibilities-1, 0, -1), [pow(self._exponent, power) for power in range(self._possibilities-1)]))

    def getValue(self):
        if not self._set:
            self._fillSet()
        arr = []
        value = 0
        for level, chance in sorted(self._set):
            value += chance
            arr.append(((level, chance), value))
        randomShot = random.randint(1, value)
        for (level, chance), value in arr:
            if randomShot <= value:
                self._set.remove((level, chance))
                return level
        return None # should be never executed

class RandomSelect(object):
    def __init__(self, possibilities):
        self._possibilities = possibilities

    def getValue(self):
        return random.randint(1, self._possibilities-1)

class RandomSelectExponential(object):
    def __init__(self, possibilities, exponent=2):
        self._possibilities = possibilities
        self._exponent = exponent

    def getValue(self):
        maxValue = pow(self._exponent, self._possibilities-1)-1
        randomShot = random.randint(1, maxValue)
        value = 0
        for level in range(1, self._possibilities):
            power = self._possibilities-level-1
            value += pow(self._exponent, power)
            if randomShot <= value:
                return level
        return None # should be never executed
