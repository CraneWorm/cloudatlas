import re
import os
import logging

def getLogger(namespace, moduleName):
    parentLogger = logging.getLogger(namespace)
    parentLogger.setLevel(logging.DEBUG)

    logger = logging.getLogger(moduleName)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s >>> %(name)s >>> %(levelname)s >>> %(message)s')

    if moduleName != '__main__':
        agentName = os.getenv('AGENT_NAME', "TESTING")

        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.ERROR)
        fileHandler = logging.FileHandler(os.path.join('log', '%s_%s.log' %(agentName, moduleName)), 'w')
        fileHandler.setLevel(logging.DEBUG)
        parentFileHandler = logging.FileHandler(os.path.join('log', '%s_%s.log' %(agentName, namespace)), 'w')
        parentFileHandler.setLevel(logging.DEBUG)

        consoleHandler.setFormatter(formatter)
        fileHandler.setFormatter(formatter)
        parentFileHandler.setFormatter(formatter)

        logger.addHandler(consoleHandler)
        logger.addHandler(fileHandler)

        # ensure that parentLogger uses parentFileHandler only once
        if namespace and not parentLogger.handlers:
            parentLogger.addHandler(parentFileHandler)
    else:
        consoleHandler = logging.StreamHandler()
        consoleHandler.setLevel(logging.DEBUG)

        consoleHandler.setFormatter(formatter)

        logger.addHandler(consoleHandler)

    return logger
