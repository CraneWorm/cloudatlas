import os
import sys
import rpyc
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA

from interpreter.parser import parser
from utils.config import Config

config = Config(__file__, 'auth.ini')


class Auth(rpyc.Service):
    def __init__(self, *args):
        super(Auth, self).__init__(*args)
        self.parser = parser.Parser(outputdir='auth')
        self.lexer = parser.Lexer(debug=0)
        self.queries_sha = set()
        self.reserved_names = {'level', 'name', 'owner', 'timestamp', 'contacts', 'cardinality', 'members'}
        with open(config.get('auth', 'private key file'), 'r') as pkey_file:
            self.pkey = RSA.importKey(pkey_file.read())

    def exposed_sign_query(self, query):
        def query_names(query):
            return [attr[2] for select in query[1] for attr in select[1] if attr[2] is not None]
        try:
            self.lexer.input(query)
            q = self.parser.parse(lexer=self.lexer, debug=0)
            sha = SHA256.new()
            sha.update(query)
            digest = sha.digest()
            names = set(query_names(q))
            if digest not in self.queries_sha:
                if names & self.reserved_names:
                    return None
                self.queries_sha.update(digest)
                self.reserved_names.update(names)
            return (query, self.pkey.sign(digest, None))
        except:
            return None


def verify_query(query, sig):
    sha = SHA256.new()
    sha.update(query)
    digest = sha.digest()
    with open(config.get('auth', 'public key file'), 'r') as public_key_file:
        return RSA.importKey(public_key_file.read()).verify(digest, sig)


def main(argv):
    from rpyc.utils.server import ThreadPoolServer
    try:
        port = None
        if len(argv) == 2:
            port = int(argv[1])
        print "::: querysigner.py, port: %s" %port
        ThreadPoolServer(service=Auth, port=port if port is not None else config.getint('auth', 'port')).start()
    finally:
        pass
        #with open(os.path.join(os.path.dirname(__file__), 'queries')):
        #    pass

if __name__ == '__main__':
    main(sys.argv)
