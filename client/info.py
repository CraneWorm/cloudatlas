import psutil
from osinfo import osinfo

from interpreter import value


def cpu_load():
    return value.ValueDouble(psutil.cpu_percent(interval=None, percpu=False))


def free_disk():
    return value.ValueInt(psutil.disk_usage('/').free)


def total_disk():
    return value.ValueInt(psutil.disk_usage('/').total)


def free_ram():
    return value.ValueInt(psutil.virtual_memory().free)


def total_ram():
    return value.ValueInt(psutil.virtual_memory().total)


def free_swap():
    return value.ValueInt(psutil.swap_memory().free)


def total_swap():
    return value.ValueInt(psutil.swap_memory().total)


def num_processes():
    return value.ValueInt(len(psutil.pids()))


def num_cores():
    return value.ValueInt(psutil.cpu_count(logical=True))


def kernel_ver():
    return value.ValueString(str(osinfo.get_os_info()[-1]))


def logged_users():
    return value.ValueInt(len(psutil.users()))


def dns_names():
    names = []
    for line in file('/etc/resolv.conf', 'r'):
        columns = line.split()
        if columns[0] == 'nameserver':
            names.extend(columns[1:])
    return value.ValueSet(map(value.ValueString, names))

INFOS = ['cpu_load', 'free_disk', 'total_disk', 'free_ram',
         'total_ram', 'free_swap', 'total_swap', 'num_processes',
         'num_cores', 'kernel_ver', 'logged_users', 'dns_names']


def info():
    print INFOS
    return {name: globals()[name]() for name in INFOS}
