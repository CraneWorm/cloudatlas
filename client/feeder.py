import sys
import rpyc
import time
import datetime
import json
from flask import Flask, render_template, abort, url_for, request

import info
from interpreter.attributesMap import AttributesMap
from interpreter import value
from agent.timer import Timer
from utils.config import Config
from agent.value_serialization import serialize, deserialize

config = Config(__file__, 'client.ini')


def datetime_to_timestamp(dt):
    return time.mktime(dt.timetuple()) * 1000 + dt.microsecond / 1000


def default_json(obj):
    if isinstance(obj, value.Value):
        return obj.val
    if isinstance(obj, set):
        return list(obj)
    if isinstance(obj, datetime.datetime):
        return datetime_to_timestamp(obj)
    if isinstance(obj, AttributesMap):
        return {k: v.val for k, v in obj}


def zmi_to_json(zmi):
    return json.dumps(zmi, default=default_json)


def fallback_contacts():
    try:
        contacts = set()
        for addr_port in config.get('agent', 'fallback contacts').split(','):
            addr, port = addr_port.split(':')
            contacts.add((str(addr), int(port)))
        return contacts
    except Exception, e:
        print e
        return None


def feed_agent(connection, timer, interval):
    infos = info.info()
    infos.update({'timestamp': value.ValueTime(datetime.datetime.now())})
    try:
        fallbacks = fallback_contacts()
        print fallbacks
        if fallbacks:
            connection.root.set_fallback_contacts(serialize(fallbacks))
        connection.root.feed_attributes(serialize(infos))
        timer.set_timeout(interval, feed_agent, connection, timer, interval)
    except Exception, e:
        print e
        raise IOError("feed_agent")


def main(argv):
    feederPort = None
    authPort = None
    wwwPort = None
    if len(argv) == 4:
        feederPort = int(argv[1])
        authPort = int(argv[2])
        wwwPort = int(argv[3])
    print "::: feeder.py, feederPort: %s, authPort: %s, wwwPort: %s" %(feederPort, authPort, wwwPort)
    agent_location = config.get('feeder', 'host'), feederPort if feederPort is not None else config.getint('feeder', 'port')
    signer_location = config.get('auth', 'host'), authPort if authPort is not None else config.getint('auth', 'port')
    interval = config.getfloat('feeder', 'interval')
    timer = Timer.start().proxy()
    try:
        connection = rpyc.connect(*agent_location, config={"allow_all_attrs": True})
        auth_conn = rpyc.connect(*signer_location, config={"allow_all_attrs": True})
        feed_agent(connection, timer, interval)
        server = Flask(__name__)
        queries = {}

        @server.route("/zmi/root")
        @server.route("/zmi/root/<path:uri_str>")
        def get_zmi(uri_str=None):
            try:
                zmi = deserialize(connection.root.get_zmi_for_uri(uri_str))
                print zmi
                return zmi_to_json(zmi)
            except Exception, e:
                print e
                return uri_str

        @server.route("/zmi/")
        def get_zmis():
            names = deserialize(connection.root.get_zmi_names())
            print names
            return zmi_to_json(names)

        @server.route("/")
        def index():
            return render_template('index.html')

        @server.route("/query", methods=['POST'])
        def add_query():
            name, data = str(request.form['name']), str(request.form['data'])
            result = auth_conn.root.sign_query(data)
            if result is None:
                return ('invalid query data', 400)
            query_str, (sig,) = result
            if connection.root.install_query(name, serialize((query_str, (str(sig), None)))):
                return (url_for('get_query', query_name=name), 200)
            return ('install query error', 400)

        @server.route("/query", methods=['GET'])
        @server.route("/query/<query_name>", methods=['GET'])
        def get_query(query_name=None):
            if query_name:
                if query_name in queries:
                    return zmi_to_json({'name': query_name, 'data': queries[query_name]})
                else:
                    abort(404)
            else:
                return zmi_to_json(queries.keys())

        server.run(port=wwwPort if wwwPort is not None else config.getint('www', 'port'))
    finally:
        print "stop!"
        timer.stop()
        connection.close()

if __name__ == '__main__':
    main(sys.argv)
