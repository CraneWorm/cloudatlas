import time
import datetime
import json
from flask import Flask

from interpreter import value

app = Flask(__name__)


def datetime_to_timestamp(dt):
    return time.mktime(dt.timetuple()) * 1000 + dt.microsecond / 1000


def default_json(obj):
    if isinstance(obj, value.Value):
        return obj.val
    if isinstance(obj, datetime.datetime):
        return datetime_to_timestamp(obj)


def zmi_to_json(zmi):
    return json.dumps({k: v.val for k, v in zmi}, default=default_json)


@app.route("/zmi")
@app.route("/zmi/<path:uri_str>")
def get_zmi(uri_str=None):
    from agent import hierarchy, zmi
    reload(zmi)
    reload(hierarchy)
    v7 = hierarchy.Hierarchy().getTreeForViolet07()
    zmi = zmi.ZMI().start().proxy()
    zmi.setAll(v7).get()
    uri = [] if uri_str is None else uri_str.split('/')
    try:
        zmi = zmi.getZmiByURI(uri).get()
        print zmi
        return zmi_to_json(zmi)
    except Exception, e:
        print e
        return uri_str

    #return zmi_to_json(zmi.getZMIByUri(uri.split('/')))
