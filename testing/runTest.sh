#!/bin/bash

MAX_ACTOR_NUMBER=9
MAX_TEST_NUMBER=999
NAMES=(one two three four five six seven eight nine)

GOSSIP_PORT=20000
FEEDER_PORT=20100
AUTH_PORT=21000
WWW_PORT=22000

AGENT_TIME=6000


function printUsage() {
    echo "Usage:"
    echo -e "\t$> ./runTest actors_number test_number"
}

function checkParams() {
    if [ $# -ne 2 ]; then
        echo "Wrong number of attributes"
        printUsage
        exit 1
    fi
    if [ $1 -le 0 ] || [ $2 -le 0 ]; then
        echo "Attributes has to be greater than 0"
        exit 1
    fi
    if [ $1 -ge $MAX_ACTOR_NUMBER ]; then
        echo "Actor number too high"
        exit 1
    fi
    if [ $2 -ge $MAX_TEST_NUMBER ]; then
        echo "Test number too high"
        exit 1
    fi
    echo "Arguments OK!"
}

function runQuerySigner() {
    python auth/querysigner.py $AUTH_PORT > log/run_query_signer.log 2>&1 &
    P="$P $!"
    echo "query_signer: $!"
}

function runAgents() {
    for i in `seq 1 $1`; do
        idx=$((i-1))
        AGENT_NAME="${NAMES[$idx]}" TEST_NUMBER=$2 python testing/runTest.py $((i+GOSSIP_PORT)) $AGENT_TIME > log/run_"${NAMES[$idx]}"_agent.log 2>&1 &
        P="$P $!"
        echo "${NAMES[$idx]}_agent: $!"
    done
}

function runFeeders() {
    for i in `seq 1 $1`; do
        idx=$((i-1))
        python client/feeder.py $((i+FEEDER_PORT)) $AUTH_PORT $((i+WWW_PORT)) > log/run_"${NAMES[$idx]}"_feeder.log 2>&1 &
        P="$P $!"
        echo "${NAMES[$idx]}_feeder: $!"
    done
}

function waitForProcesses() {
    echo "wait for: $P"
    # set reaction for signals to this process
    trap "kill $P" EXIT
    # wait if processes somehow exit by their own
    for pid in $P; do
        echo "wait: $pid"
        wait $pid
    done
}

function main() {
    checkParams "$@"
    export PYTHONPATH=`pwd`
    export ATLAS_CONFIG=testing
    runQuerySigner
    runAgents $1 $2
    sleep 1
    runFeeders $1
    waitForProcesses
    unset ATLAS_CONFIG
    unset PYTHONPATH
}

main "$@"
