import datetime

from interpreter import value
from interpreter.attributesMap import AttributesMap

class TestFunctions(object):

    def _getTestAttributesMap(self, name="/", level=0, timestamp=datetime.datetime.now(), contacts=None):
        attrM = AttributesMap()
        attrM.add("name", value.ValueString(name))
        attrM.add("level", value.ValueInt(level))
        attrM.add("timestamp", value.ValueTime(timestamp))
        attrM.add("contacts", value.ValueSet(contacts if contacts is not None else set()))
        attrM.add("cardinality", value.ValueInt(1))
        attrM.add("creation", value.ValueTime(datetime.datetime.now()))
        attrM.add("cpu_usage", value.ValueDouble(0.4))
        attrM.add("num_cores", value.ValueInt(13))
        return attrM


    def twoAgentsTwoLevels(self):
        root = self._getTestAttributesMap(name="root", level=0, timestamp=datetime.datetime.now(), contacts=set())
        one = self._getTestAttributesMap(name="one", level=1, timestamp=datetime.datetime.now(), contacts=set([]))
        two = self._getTestAttributesMap(name="two", level=1, timestamp=datetime.datetime.now(), contacts=set([]))
        three = self._getTestAttributesMap(name="three", level=1, contacts=set([('localhost', 20002)]))
        return {
            "one": [[root], [one, three]],
            "two": [[root], [two]]
        }

    def twoAgentsThreeLevels(self):
        root = self._getTestAttributesMap(name="root", level=0)
        warsaw = self._getTestAttributesMap(name="warsaw", level=1)
        berlin = self._getTestAttributesMap(name="berlin", level=1)
        one = self._getTestAttributesMap(name="one", level=2)
        two = self._getTestAttributesMap(name="two", level=2)
        three = self._getTestAttributesMap(name="three", level=2, contacts=set([('localhost', 20002)]))
        return {
            "one": [[root], [warsaw], [one, three]],
            "two": [[root], [berlin], [two]]
        }

    def fiveAgentsThreeLevels(self):
        root = self._getTestAttributesMap(name="root", level=0)
        odd = self._getTestAttributesMap(name="odd", level=1)
        even = self._getTestAttributesMap(name="even", level=1)
        one = self._getTestAttributesMap(name="one", level=2)
        two = self._getTestAttributesMap(name="two", level=2)
        three = self._getTestAttributesMap(name="three", level=2)
        four = self._getTestAttributesMap(name="four", level=2)
        five = self._getTestAttributesMap(name="five", level=2)
        contactOne = self._getTestAttributesMap(name="contactOne", level=2, contacts=set([('localhost', 20001)]))
        contactTwo = self._getTestAttributesMap(name="contactTwo", level=2, contacts=set([('localhost', 20002)]))
        contactThree = self._getTestAttributesMap(name="contactThree", level=2, contacts=set([('localhost', 20003)]))
        contactFour = self._getTestAttributesMap(name="contactFour", level=2, contacts=set([('localhost', 20004)]))
        contactFive = self._getTestAttributesMap(name="contactFive", level=2, contacts=set([('localhost', 20005)]))

        return {
            "one": [[root], [odd], [one, contactTwo]],
            "two": [[root], [even], [two, contactThree]],
            "three": [[root], [odd], [three, contactFour]],
            "four": [[root], [even], [four, contactFive]],
            "five": [[root], [odd], [five, contactOne]]
        }


    def getFunctions(self):
        return [
            self.twoAgentsTwoLevels,
            self.twoAgentsThreeLevels,
            self.fiveAgentsThreeLevels
        ]


def getDictOfTrees(testNumber):
    return TestFunctions().getFunctions()[testNumber-1]()
