import sys
import time
import datetime
import os

import pykka

_datetime = datetime.datetime
class MockDatetime(_datetime):
    def __init__(self, *args):
        super(MockDatetime, self).__init__(*args)
    @staticmethod
    def now():
        now = _datetime.now() + datetime.timedelta(seconds=int(os.getenv('OFFSET', 0)))
        return MockDatetime(now.year, now.month, now.day, now.hour, now.minute, now.second, now.microsecond, now.tzinfo)
    def __getattr__(self, attr):
        return getattr(_datetime, attr)

datetime.datetime = MockDatetime

from agent.timer import Timer
from agent.zmi import ZMI
from agent.hierarchy import Hierarchy
from agent.networking import Sender, Receiver
from agent.gossiping import Gossiper
from agent.service import Feeder

from testing.testFunctions import getDictOfTrees

from utils.config import Config
config = Config('testing', 'agent.ini')

RECEIVE_IF = "127.0.0.1"


def main(argv):
    # argv = [0: file, 1: port, 2: seconds]
    try:
        if len(argv) != 3:
            raise Exception("Wrong number of arguments")
        port = int(argv[1])
        seconds = int(argv[2])
        agentName = os.getenv('AGENT_NAME', "AGENT")
        testNumber = os.getenv('TEST_NUMBER', "TEST")

        forest = getDictOfTrees(int(testNumber))
        if agentName not in forest:
            raise Exception("Wrong agentName: %s" %agentName)

        print "::: runTest.py, port: %s, seconds: %s, agentNumber: %s, testNumber: %s" %(port, seconds, agentName, testNumber)
        timer = Timer.start().proxy()
        zmi = ZMI.start(timer=timer).proxy()
        zmi.setAll(forest[agentName])
        sender = Sender.start().proxy()
        gossiper = Gossiper.start(port=port, timer=timer, sender=sender, zmi=zmi).proxy()
        routes = [(gossiper.routeFilter, gossiper.routeReceiver)]
        Receiver.start(port=port, interface=RECEIVE_IF, routing=routes, timer=timer)
        Feeder.start(zmi=zmi, port=port+100).proxy()

        time.sleep(seconds)

    except Exception as e:
        import traceback
        traceback.print_exc()
        print "exception in runTest.py: %s" %e
    finally:
        pykka.ActorRegistry.stop_all()

if __name__ == "__main__":
    main(sys.argv)
