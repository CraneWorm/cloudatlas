#!/bin/bash

export PYTHONPATH=`pwd`;

python auth/querysigner.py &
pids="$pids $!"

ATLAS_CONFIG=odd_even/one python agent/runAgent.py 3600 > /tmp/one.agent 2>&1 &
pids="$pids $!"
ATLAS_CONFIG=odd_even/two python agent/runAgent.py 3600 > /tmp/two.agent 2>&1 &
pids="$pids $!"
ATLAS_CONFIG=odd_even/three python agent/runAgent.py 3600 > /tmp/three.agent 2>&1 &
pids="$pids $!"
ATLAS_CONFIG=odd_even/four python agent/runAgent.py 3600 > /tmp/four.agent 2>&1 &
pids="$pids $!"
ATLAS_CONFIG=odd_even/five python agent/runAgent.py 3600 > /tmp/five.agent 2>&1 &
pids="$pids $!"

sleep 1;

ATLAS_CONFIG=odd_even/one python client/feeder.py > /tmp/one.client 2>&1 &
pids="$pids $!"
ATLAS_CONFIG=odd_even/two python client/feeder.py > /tmp/two.client 2>&1 &
pids="$pids $!"
ATLAS_CONFIG=odd_even/three python client/feeder.py > /tmp/three.client 2>&1 &
pids="$pids $!"
ATLAS_CONFIG=odd_even/four python client/feeder.py > /tmp/four.client 2>&1 &
pids="$pids $!"
ATLAS_CONFIG=odd_even/five python client/feeder.py > /tmp/five.client 2>&1 &
pids="$pids $!"

unset PYTHONPATH;
trap "kill ${pids}; exit 1" INT TERM

for pid in $pids
do
    wait $pid;
done
