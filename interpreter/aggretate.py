from collections import defaultdict
import value
import random, math
from datetime import datetime

class AggregateFunction(object):

    def __init__(self):
        self._aggregates = defaultdict(self.noAggregateFunction)
        self._aggregates["avg"] = self.avg
        self._aggregates["count"] = self.count
        self._aggregates["sum"] = self.sum
        self._aggregates["first"] = self.first
        self._aggregates["last"] = self.last
        self._aggregates["random"] = self.random
        self._aggregates["min"] = self.min
        self._aggregates["max"] = self.max
        self._aggregates["land"] = self.land
        self._aggregates["lor"] = self.lor

        self._aggregates["distinct"] = self.distinct
        self._aggregates["unfold"] = self.unfold

        self._aggregates["lnot"] = self.lnot
        self._aggregates["isNull"] = self.isNull
        self._aggregates["now"] = self.now
        self._aggregates["epoch"] = self.epoch
        self._aggregates["size"] = self.size
        self._aggregates["round"] = self.round
        self._aggregates["floor"] = self.floor
        self._aggregates["ceil"] = self.ceil

    def _isNull(self, x):
        return True if isinstance(x, value.ValueNone) or (x.val is None) else False

    def _filterNulls(self, arr):
        return [elem for elem in arr if not self._isNull(elem)]

    def run(self, name):
        return self._aggregates[name]

    def noAggregateFunction(self):
        raise NotImplementedError("aggregate function not found")

    def avg(self, args, evalExp):
        res = evalExp(args[0]).val
        arr = self._filterNulls(res)
        if not arr:
            return value.OneResult(value.ValueNone())
        head = arr[0]
        tail = arr[1:]
        res = sum(tail, head)
        return value.OneResult(res/res.__class__(len(arr)))

    def count(self, args, evalExp):
        res = evalExp(args[0]).val
        arr = self._filterNulls(res)
        return value.OneResult(value.ValueInt(len(arr)))

    def sum(self, args, evalExp):
        res = evalExp(args[0]).val
        arr = self._filterNulls(res)
        if not arr:
            return value.OneResult(value.ValueNone())
        head = arr[0]
        tail = arr[1:]
        res = sum(tail, head)
        return value.OneResult(res)

    def first(self, args, evalExp):
        n = evalExp(args[0]).val.val
        res = evalExp(args[1]).val
        arr = self._filterNulls(res)
        if len(arr) < n:
            return value.OneResult(value.ValueList(arr))
        return value.OneResult(value.ValueList(arr[:n]))

    def last(self, args, evalExp):
        n = evalExp(args[0]).val.val
        res = evalExp(args[1]).val
        arr = self._filterNulls(res)
        if len(arr) < n:
            return value.OneResult(value.ValueList(arr))
        return value.OneResult(value.ValueList(arr[-n:]))

    def random(self, args, evalExp):
        n = evalExp(args[0]).val.val
        res = evalExp(args[1]).val
        arr = self._filterNulls(res)
        if len(arr) < n:
            return value.OneResult(value.ValueList(arr))
        rand = random.sample(range(len(arr)), n)
        sort = sorted(rand)
        return value.OneResult(value.ValueList([arr[r] for r in sort]))

    def min(self, args, evalExp):
        res = evalExp(args[0]).val
        if not res:
            return value.OneResult(value.ValueNone())
        arr = self._filterNulls(res)
        if arr:
            return value.OneResult(min(arr))
        else:
            return value.OneResult(value.ValueNone())

    def max(self, args, evalExp):
        res = evalExp(args[0]).val
        if not res:
            return value.OneResult(value.ValueNone())
        arr = self._filterNulls(res)
        if arr:
            return value.OneResult(max(arr))
        else:
            return value.OneResult(value.ValueNone())

    def land(self, args, evalExp):
        res = evalExp(args[0]).val
        if not res:
            return value.OneResult(value.ValueBoolean(True))
        arr = self._filterNulls(res)
        if not arr:
            return value.OneResult(value.ValueNone())
        acc = value.ValueBoolean(True)
        for elem in arr:
            acc = acc and elem
        return value.OneResult(acc)

    def lor(self, args, evalExp):
        res = evalExp(args[0]).val
        if not res:
            return value.OneResult(value.ValueBoolean(False))
        arr = self._filterNulls(res)
        if not arr:
            return value.OneResult(value.ValueNone())
        acc = value.ValueBoolean(False)
        for elem in arr:
            acc = acc or elem
        return value.OneResult(acc)


    def distinct(self, args, evalExp):
        res = evalExp(args[0])
        arr = []
        tmpSet = set()
        for elem in res:
            if elem not in tmpSet:
                tmpSet.add(elem)
                arr.append(elem)
        return value.ListResult(arr)

    def unfold(self, args, evalExp):
        res = evalExp(args[0])
        return value.ListResult([elem for ll in self._filterNulls(res) for elem in ll])


    def lnot(self, args, evalExp):
        x = evalExp(args[0])
        return value.ValueBoolean(not x)

    def isNull(self, args, evalExp):
        x = evalExp(args[0])
        return value.ValueBoolean(self._isNull(x))

    def now(self, args, evalExp):
        return value.OneResult(value.ValueTime(datetime.now()))

    def epoch(self, args, evalExp):
        return value.OneResult(value.ValueTime(datetime(2000, 1, 1, 0, 0, 0, 0)))

    def size(self, args, evalExp):
        x = evalExp(args[0])
        return x.map(lambda obj: value.ValueInt(len(obj)))

    def round(self, args, evalExp):
        x = evalExp(args[0])
        return x.map(lambda obj: value.ValueDouble(round(obj.val)))

    def floor(self, args, evalExp):
        x = evalExp(args[0])
        return x.map(lambda obj: value.ValueDouble(math.floor(obj.val)))

    def ceil(self, args, evalExp):
        x = evalExp(args[0])
        return x.map(lambda obj: value.ValueDouble(math.ceil(obj.val)))
