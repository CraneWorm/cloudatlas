from datetime import datetime, timedelta
from itertools import izip as zip
from collections import defaultdict
import operator as op


def instance(op_name, function):
    def decorator(cls):
        def un_op(self):
            if isinstance(self, ValueNone):
                return ValueNone()
            else:
                return function(self)
        setattr(cls, op_name, un_op)
        return cls
    return decorator

def instance2(op_name, function):
    def decorator(cls):
        def bin_op(self, other):
            if isinstance(self, ValueNone) or isinstance(other, ValueNone):
                return ValueNone()
            if isinstance(other, cls):
                return function(self, other)
            else:
                raise TypeError("Expected value of type %s, got %s." % (cls.__name__, other.__class__.__name__))
        setattr(cls, op_name, bin_op)
        return cls
    return decorator

def isNone(value):
    return isinstance(value, ValueNone) or (isinstance(value, Value) and value.val == None)

def same_type_or_none(bin_op):
    def strict(self, other):
        if isNone(self) or isNone(other):
            return ValueNone()
        if isinstance(other, self.__class__):
            return bin_op(self, other)
        else:
            return NotImplemented
    return strict

def value_or_none(un_op):
    def strict(self):
        if isNone(self):
            return ValueNone()
        else:
            return un_op(self)
    return strict


class Value(object):
    __slots__ = ['val']

    def __init__(self, val):
        self.val = val

    def __repr__(self):
        return "%s %s" % (self.__class__.__name__, self.val)

    def __hash__(self):
        return hash((self.__class__.__name__, self.val))

    def __nonzero__(self):
        return True if self.val else False


class Eq(Value):
    @same_type_or_none
    def __eq__(self, other):
        return ValueBoolean(self.val == other.val)

    @same_type_or_none
    def __ne__(self, other):
        return ValueBoolean(self.val != other.val)


class Cmp(Eq):
    @same_type_or_none
    def __gt__(self, other):
        return ValueBoolean(self.val > other.val)

    @same_type_or_none
    def __lt__(self, other):
        return ValueBoolean(self.val < other.val)

    @same_type_or_none
    def __ge__(self, other):
        return ValueBoolean(self.val >= other.val)

    @same_type_or_none
    def __le__(self, other):
        return ValueBoolean(self.val <= other.val)

    def __cmp__(self, other):
        if isNone(self):
            return isNone(other) - 1
        else:
            return isNone(other) or cmp(self.val, other.val)


class Numeric(Cmp):
    @same_type_or_none
    def __add__(self, other):
        return self.__class__(self.val + other.val)

    @same_type_or_none
    def __sub__(self, other):
        return self.__class__(self.val - other.val)

    @same_type_or_none
    def __mul__(self, other):
        return self.__class__(self.val * other.val)

    def __div__(self, other):
        if isinstance(other, self.__class__):
            return ValueDouble(float(self.val) / other.val)
        elif isNone(other):
            return ValueNone()
        else:
            return NotImplemented

    def __rdiv__(self, other):
        if isinstance(other, self.__class__):
            return ValueDouble(float(self.val) / other.val)
        elif isNone(other):
            return ValueNone()
        else:
            return NotImplemented

    def __mod__(self, other):
        if isinstance(other, self.__class__):
            return self.__class__(self.val % other.val)
        elif isNone(other):
            return ValueNone()
        else:
            return NotImplemented

    @value_or_none
    def __neg__(self):
        return self.__class__(-self.val)


class ValueBoolean(Eq):
    @same_type_or_none
    def __and__(self, other):
        return ValueBoolean(self.val and other.val)

    @same_type_or_none
    def __or__(self, other):
        return ValueBoolean(self.val or other.val)

    @value_or_none
    def __not__(self):
        return ValueBoolean(not self.val)

    def __nonzero__(self):
        return self.val


class ValueInt(Numeric):
    pass


class ValueDouble(Numeric):
    pass


class ValueContact(Eq):
    pass


class ValueDuration(Numeric):
    def __init__(self, val):
        if val is None:
            self.val = None
        elif isinstance(val, timedelta):
            super(ValueDuration, self).__init__(val)
        elif isinstance(val, str):
            from conversions import stringToTimedelta
            self.val = stringToTimedelta(val)
        else:
            self.val = timedelta(seconds=val)

    def __mul__(self, other):
        if isNone(other):
            return ValueNone()
        if isinstance(other, ValueDuration):
            return ValueDuration(timedelta(seconds=self.val.total_seconds() * other.val.total_seconds()))
        if isinstance(other, Numeric):
            return ValueDuration(timedelta(seconds=self.val.total_seconds() * other.val))
        else:
            return NotImplemented

    def __rmul__(self, other):
        return self.__mul__(other)

    def __div__(self, other):
        if isNone(other):
            return ValueNone()
        if isinstance(other, ValueDuration):
            return ValueDuration(timedelta(seconds=self.val.total_seconds() / other.val.total_seconds()))
        if isinstance(other, Numeric):
            return ValueDuration(timedelta(seconds=self.val.total_seconds() / other.val))
        else:
            return NotImplemented


class ValueList(Eq):
    @same_type_or_none
    def __add__(self, other):
        return ValueList(self.val + other.val)

    def __len__(self):
        return len(self.val)

    def __iter__(self):
        return iter(self.val)


class ValueSet(Eq):
    @same_type_or_none
    def __add__(self, other):
        return ValueSet(self.val | other.val)

    def __len__(self):
        return len(self.val)

    def __iter__(self):
        return iter(self.val)


class ValueNone(Numeric, ValueBoolean):
    def __init__(self):
        super(ValueNone, self).__init__(None)

    def __eq__(self, other):
        return ValueBoolean(isNone(other))

    def __ne__(self, other):
        return ValueBoolean(not isNone(other))

    def __nonzero__(self):
        return False

    # def __len__(self):
    #     raise ValueError('ValueNone used in some util function')


class ValueString(Cmp):
    @same_type_or_none
    def __add__(self, other):
        return ValueString(self.val + other.val)

    def __len__(self):
        return len(self.val)

    def __repr__(self):
        return str(self.val)


class ValueQuery(ValueString): pass


class ValueTime(Cmp):
    DATE_FORMAT = '%Y/%m/%d %H:%M:%S.%f'
    def __init__(self, val):
        super(ValueTime, self).__init__(None)
        if isinstance(val, datetime):
            self.val = val
        else:
            self.val = datetime.strptime(val, self.DATE_FORMAT) if val else None

    def __add__(self, other):
        if isNone(other):
            return ValueNone()
        if isinstance(other, ValueDuration):
            return ValueTime(self.val + other.val)
        else:
            return NotImplemented

    def __sub__(self, other):
        if isNone(other):
            return ValueNone()
        if isinstance(other, ValueTime):
            return ValueDuration(self.val - other.val)
        else:
            return NotImplemented

bin_ops = [op.__add__, op.__mul__,
           op.__sub__, op.__div__,
           op.__mod__, op.__or__,
           op.__and__, op.__gt__,
           op.__lt__,  op.__ge__,
           op.__le__,  op.__eq__,
           op.__ne__]

un_ops = [op.__neg__, op.__not__]


class ValueResult(object):
    def __init__(self, val):
        self.val = val

    def __repr__(self):
        return "%s %s" % (self.__class__.__name__, self.val)


class IterableResult(ValueResult):

    def __len__(self):
        return len(self.val)

    def __iter__(self):
        return iter(self.val)

    def __nonzero__(self):
        return len(self)

    def map(self, callable):
        return self.__class__([callable(item) if not isNone(item) else ValueNone() for item in self.val])

class ColumnResult(IterableResult): pass
class ListResult(IterableResult): pass


class OneResult(ValueResult):
    def __nonzero__(self):
        return self.val.__nonzero__()

    def map(self, callable):
        return self.__class__(callable(self.val) if not isNone(self.val) else ValueNone())


class ResultsMapper(object):

    def __init__(self):
        self.actions = defaultdict(ResultsMapper.not_implemented)

        self.actions.update({
            (OneResult, OneResult):
                lambda bin_op, r1, r2: OneResult(bin_op(r1.val, r2.val)),
            (OneResult, ColumnResult):
                lambda bin_op, r1, r2: ColumnResult([bin_op(r1.val, val) for val in r2.val]),
            (ColumnResult, OneResult):
                lambda bin_op, r1, r2: ColumnResult([bin_op(val, r2.val) for val in r1.val]),
            (OneResult, ListResult):
                lambda bin_op, r1, r2: ListResult([bin_op(r1.val, val) for val in r2.val]),
            (ListResult, OneResult):
                lambda bin_op, r1, r2: ListResult([bin_op(val, r2.val) for val in r1.val]),
            (ColumnResult, ColumnResult):
                lambda bin_op, r1, r2: ColumnResult([bin_op(*args) for args in  zip(r1.val, r2.val)]),
            (ColumnResult, None.__class__):
                lambda un_op, r1: ColumnResult([un_op(val) for val in r1.val]),
            (ListResult, None.__class__):
                lambda un_op, r1: ListResult([un_op(val) for val in r1.val]),
            (OneResult, None.__class__):
                lambda un_op, val: OneResult(un_op(val))
        })

    @staticmethod
    def not_implemented(bin_op, r1, r2):
        return NotImplemented

    def __call__(self, bin_op, r1, r2=None):
        return self.actions.get((r1.__class__, r2.__class__))(bin_op, r1, r2)

for cls in [ColumnResult, ListResult, OneResult]:
    for bin_op in bin_ops:
        # python's scoping rules make me use a lambda to avoid overwriting all functions with last bin_op
        (lambda bin_op: setattr(cls, bin_op.__name__, lambda arg_self, arg_other: ResultsMapper()(bin_op, arg_self, arg_other)))(bin_op)
    for un_op in un_ops:
        (lambda un_op: setattr(cls, un_op.__name__, lambda arg_self, arg_other: ResultsMapper()(un_op, arg_self)))(un_op)
