from ZMI import *
from attribute import *
from value import *

class Hierarchy(object):

    def __init__(self): pass

    def fill(self):
        # /
        attrM = AttributesMap()
        attrM.add("level", ValueInt(0))
        attrM.add("name", ValueString(None))
        attrM.add("owner", ValueString("/uw/violet07"))
        attrM.add("timestamp", ValueTime("2012/11/09 20:10:17.342"))
        attrM.add("contacts", ValueSet(set()))
        attrM.add("cardinality", ValueInt(0))
        self.root = ZMI(None, attrM)
        
        # /uw
        attrM = AttributesMap()
        attrM.add("level", ValueInt(1))
        attrM.add("name", ValueString("uw"))
        attrM.add("owner", ValueString("/uw/violet07"))
        attrM.add("timestamp", ValueTime("2012/11/09 20:08:13.123"))
        attrM.add("contacts", ValueSet(set()))
        attrM.add("cardinality", ValueInt(0))
        self.uw = ZMI(self.root, attrM)
        
        # /pjwstk
        attrM = AttributesMap()
        attrM.add("level", ValueInt(1))
        attrM.add("name", ValueString("pjwstk"))
        attrM.add("owner", ValueString("/pjwstk/whatever01"))
        attrM.add("timestamp", ValueTime("2012/11/09 20:08:13.123"))
        attrM.add("contacts", ValueSet(set()))
        attrM.add("cardinality", ValueInt(0))
        self.pjwstk = ZMI(self.root, attrM)
        
        # /uw/violet07
        attrM = AttributesMap()
        attrM.add("level", ValueInt(2))
        attrM.add("name", ValueString("violet07"))
        attrM.add("owner", ValueString("/uw/violet07"))
        attrM.add("timestamp", ValueTime("2012/11/09 18:00:00.000"))
        attrM.add("contacts", ValueSet(set([ValueString("UW1A"), ValueString("UW1B"), ValueString("UW1C")])))
        attrM.add("cardinality", ValueInt(1))
        attrM.add("members", ValueSet(set([ValueString("UW1")])))
        attrM.add("creation", ValueTime("2011/11/09 20:08:13.123"))
        attrM.add("cpu_usage", ValueDouble(0.9))
        attrM.add("num_cores", ValueInt(3))
        attrM.add("has_ups", ValueBoolean(None))
        attrM.add("some_names", ValueList([ValueString("tola"), ValueString("tosia")]))
        attrM.add("expiry", ValueDuration("+13 12:00:00.000"))
        violet07 = ZMI(self.uw, attrM)
        
        # /uw/khaki31
        attrM = AttributesMap()
        attrM.add("level", ValueInt(2))
        attrM.add("name", ValueString("khaki31"))
        attrM.add("owner", ValueString("/uw/khaki31"))
        attrM.add("timestamp", ValueTime("2012/11/09 20:03:00.000"))
        attrM.add("contacts", ValueSet(set([ValueString("UW2A"), ValueString("PJ1")])))
        attrM.add("cardinality", ValueInt(1))
        attrM.add("members", ValueSet(set([ValueString("UW2A")])))
        attrM.add("creation", ValueTime("2011/11/09 20:12:13.123"))
        attrM.add("cpu_usage", ValueDouble(None))
        attrM.add("num_cores", ValueInt(3))
        attrM.add("has_ups", ValueBoolean(False))
        attrM.add("some_names", ValueList([ValueString("agatka"), ValueString("beatka"), ValueString("celina")]))
        attrM.add("expiry", ValueDuration("-13 11:00:00.000"))
        khaki31 = ZMI(self.uw, attrM)
        
        # /uw/khaki13
        attrM = AttributesMap()
        attrM.add("level", ValueInt(2))
        attrM.add("name", ValueString("khaki13"))
        attrM.add("owner", ValueString("/uw/khaki13"))
        attrM.add("timestamp", ValueTime("2012/11/09 21:03:00.000"))
        attrM.add("contacts", ValueSet(set([])))
        attrM.add("cardinality", ValueInt(1))
        attrM.add("members", ValueSet(set([ValueString("UW3B")])))
        attrM.add("creation", ValueTime(None))
        attrM.add("cpu_usage", ValueDouble(0.1))
        attrM.add("num_cores", ValueInt(None))
        attrM.add("has_ups", ValueBoolean(True))
        attrM.add("some_names", ValueList([]))
        attrM.add("expiry", ValueDuration(None))
        khaki13 = ZMI(self.uw, attrM)
        
        # /pjwstk/whatever01
        attrM = AttributesMap()
        attrM.add("level", ValueInt(2))
        attrM.add("name", ValueString("whatever01"))
        attrM.add("owner", ValueString("/pjwstk/whatever01"))
        attrM.add("timestamp", ValueTime("2012/11/09 21:12:00.000"))
        attrM.add("contacts", ValueSet(set([ValueString("UW1"), ValueString("PJ1")])))
        attrM.add("cardinality", ValueInt(1))
        attrM.add("members", ValueSet(set([ValueString("PJ1")])))
        attrM.add("creation", ValueTime("2012/10/18 07:03:00.000"))
        attrM.add("cpu_usage", ValueDouble(0.1))
        attrM.add("num_cores", ValueInt(7))
        attrM.add("php_modules", ValueList([ValueString("rewrite")]))
        whatever01 = ZMI(self.pjwstk, attrM)
        
        # /pjwstk/whatever02
        attrM = AttributesMap()
        attrM.add("level", ValueInt(2))
        attrM.add("name", ValueString("whatever02"))
        attrM.add("owner", ValueString("/pjwstk/whatever02"))
        attrM.add("timestamp", ValueTime("2012/11/09 21:13:00.000"))
        attrM.add("contacts", ValueSet(set([ValueString("UW3"), ValueString("PJ2")])))
        attrM.add("cardinality", ValueInt(1))
        attrM.add("members", ValueSet(set([ValueString("PJ2")])))
        attrM.add("creation", ValueTime("2012/10/18 07:04:00.000"))
        attrM.add("cpu_usage", ValueDouble(0.4))
        attrM.add("num_cores", ValueInt(13))
        attrM.add("php_modules", ValueList([ValueString("odbc")]))
        whatever02 = ZMI(self.pjwstk, attrM)
