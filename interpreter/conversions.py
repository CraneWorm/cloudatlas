import re
from datetime import timedelta, date
from value import *

__all__ = ['to_boolean', 'to_double', 'to_duration', 'to_integer', 'to_list', 'to_set', 'to_string', 'to_time']

class Conversions(object):

    def cast_or_none(self, conversion, value):
        try:
            func = getattr(value, conversion)
            return func()
        except:
            return ValueNone()

    def __call__(self, conversion, value):
        return self.cast_or_none(conversion, value)


conversions = ['to_boolean', 'to_double', 'to_duration', 'to_integer', 'to_list', 'to_set', 'to_string', 'to_time']

def timedeltaToString(delta):
    if delta.total_seconds == 0:
        return "+0"
    hours, remainder = divmod(delta.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    milliseconds = delta.microseconds / 10**3
    return "%+d %02d:%02d:%02d.%03d" % (delta.days, hours, minutes, seconds, milliseconds)

def stringToTimedelta(string):
    DURATION_REGEX = re.compile("^([\-|\+]\d+) (\d\d):(\d\d):(\d\d)\.(\d\d\d)$")
    if string == "+0":
        return timedelta()
    keys = ['days', 'hours', 'minutes', 'seconds', 'milliseconds']
    values = map(int, DURATION_REGEX.match(string).groups())
    return timedelta(**dict(zip(keys, values)))

ValueInt.to_string = lambda self: ValueString(str(self.val))
ValueInt.to_double = lambda self: ValueDouble(float(self.val))
ValueInt.to_duration = lambda self: ValueDuration(timedelta(milliseconds=self.val))
ValueInt.to_integer = lambda self: self

ValueBoolean.to_string = lambda self: ValueString(str(self.val).lower())

ValueDouble.to_string = lambda self: ValueString(str(self.val).lower())
ValueDouble.to_integer = lambda self: ValueInt(int(self.val))
ValueDouble.to_double = lambda self: self

ValueList.to_set = set

ValueSet.to_list = list

ValueTime.to_string = lambda self: ValueString(self.val.strftime(ValueTime.DATE_FORMAT)[:-3])

ValueDuration.to_string = lambda self: ValueString(timedeltaToString(self.val))

ValueContact.to_string = lambda self: ValueString(str(self.val))

ValueList.to_string = lambda self: ValueString("[%s]" % ", ".join([el.__repr__() for el in self.val]))
ValueSet.to_string = lambda self: ValueString(str(self.val))

ValueString.to_boolean = lambda self: ValueBoolean({'true': True, 'false': False}[self.val])
ValueString.to_integer = lambda self: ValueInt(int(self.val))
ValueString.to_double = lambda self: ValueDouble(float(self.val))
ValueString.to_time = lambda self: ValueTime(datetime.strptime(self.val, ValueTime.DATE_FORMAT))
ValueString.to_duration = lambda self: ValueDuration(stringToTimedelta(self.val))
ValueString.to_string = lambda self: self

for conversion in conversions:
    (lambda conversion:
        setattr(OneResult, conversion,
            lambda self: OneResult(Conversions()(conversion, self.val))))(conversion)

for conversion in conversions:
    (lambda conversion:
        setattr(ColumnResult, conversion,
            lambda self: ColumnResult([Conversions()(conversion, val) for val in self.val])))(conversion)
