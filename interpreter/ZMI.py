from attributesMap import *

class ZMI(object):
    def __init__(self, parent=None, attributes=None):
        self.attributes = attributes or AttributesMap()
        self.parent = parent
        self.__dict__['children'] = []

    def cloneAttributes(self):
        return self.attributes.clone()

    def updateAttributes(self, attrM):
        self.attributes = attrM

    @property
    def parent(self):
        return self.__dict__['parent'] if isinstance(self.__dict__['parent'], ZMI) else None

    @parent.setter
    def parent(self, parent):
        if hasattr(self, 'parent') and isinstance(self.parent, ZMI):
            self.parent.children.remove(self)
        if isinstance(parent, ZMI):
            parent.children.append(self)
        self.__dict__['parent'] = parent

    @property
    def children(self): return self.__dict__['children']
