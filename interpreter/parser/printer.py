from collections import deque
import visitor as V
from AST import *

class QueryPrinter(object):
    @V.on('node')
    def visit(self, node, *args):
        pass

    @V.when(Program)
    def visit(self, node, statements):
        self.parts = deque()
        for statement in statements:
            self.visit(*statement)
        return ' '.join(self.parts)

    @V.when(Select)
    def visit(self, node, columns, where=None, order_by=None):
        self.parts.append('SELECT')
        for column in columns:
            self.visit(*column)
        if where:
            self.parts.append('WHERE')
            self.visit(*where)
        if order_by:
            self.parts.append('ORDER BY')
            it = iter(order_by)
            self.visit(*it.next())
            for order in it:
                self.parts.append(self.parts.pop() +',')
                self.visit(*order)

    @V.when(Ordering)
    def visit(self, node, expression, order, nulls):
        self.visit(*expression)
        if order:
            self.parts.append(order)
        if nulls:
            self.parts.append('NULLS')
            self.parts.append(nulls)

    @V.when(ListSelectItem)
    def visit(self, node, expression, alias=None):
        self.visit(*expression)
        if alias:
            self.parts.append('AS')
            self.parts.append(alias)

    @V.when(Binary)
    def visit(self, node, left, right, op):
        self.visit(*left)
        self.parts.append(op)
        self.visit(*right)

    @V.when(Value)
    def visit(self, node, value):
        self.parts.append(str(value))

    @V.when(RegexpMatch)
    def visit(self, node, string, regexp):
        self.visit(*string)
        self.parts.append('REGEXP')
        self.visit(*regexp)

    @V.when(CallExpr)
    def visit(self, node, name, args):
        self.parts.append(name + '(')
        for arg in args:
            self.visit(*arg)
        self.parts.append(')')