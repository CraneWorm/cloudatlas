import ply.lex as lex
from interpreter.value import *

class Lexer(object):

    reserved = {'SELECT', 'AS', 'WHERE', 'ORDER', 'BY',
    'ASC', 'DESC', 'FIRST', 'LAST', 'NULLS',
    'AND', 'OR', 'NOT', 'REGEXP'}

    boolean = {'true': True, 'false': False}

    tokens = [
        'PLUS','MINUS','TIMES','DIVIDE','MODULO',
        'EQ', 'GT', 'LT', 'GE', 'LE', 'NE',
        'LPAREN','RPAREN', 'COMA', 'SEMI',
        'STRING', 'DOUBLE', 'NUMBER', 'BOOL', 'NAME'
        ] + list(reserved)

    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_TIMES = r'\*'
    t_DIVIDE = r'/'
    t_MODULO = r'%'
    t_LPAREN  = r'\('
    t_RPAREN  = r'\)'
    t_COMA = r','
    t_SEMI = r';'
    t_EQ = r'='
    t_NE = r'<>'
    t_GT = r'>'
    t_LT = r'<'
    t_GE = r'>='
    t_LE = r'<='

    states = (('string', 'exclusive'),
              ('escaped', 'exclusive')
    )

    def t_DOUBLE(self, t):
        r'[0-9]*\.[0-9]+([eE]-?[0-9]+)?|[0-9]+([eE]-?[0-9]+)'
        t.value = ValueDouble(float(t.value))
        return t

    def t_NUMBER(self, t):
        r'0|[1-9][0-9]*'
        t.value = ValueInt(int(t.value))
        return t

    def t_NAME(self, t):
        r'([a-zA-Z]|&)[0-9a-zA-Z_]*'
        t.type, t.value = (t.value in self.reserved and (t.value, t.value)) or \
                          (t.value in self.boolean.keys() and ('BOOL', ValueBoolean(self.boolean[t.value]))) or \
                          ('NAME', t.value)
        return t

    def t_STRING(self, t):
        r'"'
        t.lexer.string_begin = t.lexer.lexpos-1
        t.lexer.begin('string')

    def t_string_backslash(self, t):
        r'\\'
        t.lexer.begin('escaped')

    def t_string_end(self, t):
        r'"'
        t.type = 'STRING'
        t.value = ValueString(t.lexer.lexdata[t.lexer.string_begin+1:t.lexer.lexpos-1])
        t.lexer.begin('INITIAL')
        return t

    def t_string_char(self, t):
        r'.'

    def t_escaped_char(self, t):
        r'.'
        t.lexer.begin('string')

    # Define a rule so we can track line numbers
    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    # A string containing ignored characters (spaces and tabs)
    t_ignore  = ' \t'
    t_string_ignore = ''
    t_escaped_ignore = ''

    # Error handling rule
    def t_error(self, t):
        print "Illegal character '%s'" % t.value[0]
        t.lexer.skip(1)

    def input(self, *args):
        self.lexer.input(*args)

    def __init__(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)
        self.token = self.lexer.token

    def __iter__(self):
        for token in self.lexer:
            self.token = token
            yield token

