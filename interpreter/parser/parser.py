import ply.yacc as yacc

from collections import deque
from lexer import Lexer
import AST


class ParserException(Exception):
    pass


class Parser(object):
    tokens = Lexer.tokens

    def p_program(self, p):
        'program : listStatement'
        p[0] = (AST.Program(), p[1])

    def p_listStatement(self, p):
        '''listStatement : statement SEMI listStatement
                         | statement'''
        p[0] = p[3] if len(p) > 2 else deque()
        p[0].appendleft(p[1])

    def p_statement(self, p):
        'statement : SELECT listSelItem whereClause orderByClause'
        p[0] = (AST.Select(), p[2], p[3], p[4])

    def p_listSelItem(self, p):
        '''listSelItem : selItem COMA listSelItem
                       | selItem'''
        p[0] = p[3] if len(p) > 2 else deque()
        p[0].appendleft(p[1])

    def p_selItem(self, p):
        '''selItem : condExpr
                   | condExpr AS NAME'''
        name = p[3] if len(p) > 2 else None
        p[0] = (AST.ListSelectItem(), p[1], name)

    def p_condExpr(self, p):
        '''condExpr : condExpr OR condExpr1
                    | condExpr1'''
        p[0] = (AST.BinaryOr(), p[1], p[3], p[2]) if len(p) > 3 else p[1]

    def p_condExpr1(self, p):
        '''condExpr1 : condExpr1 AND condExpr2
                     | condExpr2'''
        p[0] = (AST.BinaryAnd(), p[1], p[3], p[2]) if len(p) > 3 else p[1]

    def p_condExpr2(self, p):
        '''condExpr2 : NOT condExpr2
                     | boolExpr'''
        p[0] = (AST.UnaryBoolean(), p[2]) if len(p) > 2 else p[1]

    def p_boolExpr(self, p):
        '''boolExpr : basicExpr relOp basicExpr
                    | basicExpr REGEXP STRING
                    | basicExpr'''
        if len(p) > 2:
            p[0] = (AST.Comparison(), p[1], p[3], p[2]) if p[2] != 'REGEXP' else (AST.RegexpMatch(), p[1], (AST.Value(), p[3]))
        else:
            p[0] = p[1]

    def p_basicExpr(self, p):
        '''basicExpr : basicExpr PLUS basicExpr1
                     | basicExpr MINUS basicExpr1
                     | basicExpr1'''
        p[0] = (AST.BinaryArith(), p[1], p[3], p[2]) if len(p) > 3 else p[1]

    def p_basicExpr1(self, p):
        '''basicExpr1 : basicExpr1 TIMES basicExpr2
                      | basicExpr1 DIVIDE basicExpr2
                      | basicExpr1 MODULO basicExpr2
                      | basicExpr2'''
        p[0] = (AST.BinaryArith(), p[1], p[3], p[2]) if len(p) > 3 else p[1]

    def p_basicExpr2(self, p):
        '''basicExpr2 : MINUS basicExpr2
                      | basicExpr3'''
        p[0] = (AST.UnaryArith(), p[2], p[1]) if len(p) > 2 else p[1]

    def p_basicExpr3(self, p):
        '''basicExpr3 : BOOL
                      | NAME
                      | NAME LPAREN listCondExpr RPAREN
                      | STRING
                      | NUMBER
                      | DOUBLE
                      | LPAREN condExpr RPAREN
                      | LPAREN statement RPAREN'''
        p[0] = (AST.Value(), p[1]) if len(p) < 4 else \
                p[2] if len(p) < 5 else (AST.CallExpr(), p[1], p[3])

    def p_relOp(self, p):
        '''relOp : EQ
                 | NE
                 | GT
                 | LT
                 | GE
                 | LE'''
        p[0] = p[1]

    def p_listCondExpr(self, p):
        '''listCondExpr : empty
                        | condExpr
                        | condExpr COMA listCondExpr'''
        p[0] = p[3] if len(p) > 2 else deque() if len(p) > 1 else None
        if len(p) > 1:
            p[0].appendleft(p[1])

    def p_whereClause(self, p):
        '''whereClause : empty
                       | WHERE condExpr'''
        p[0] = None if len(p) < 3 else p[2]

    def p_orderByClause(self, p):
        '''orderByClause : empty
                         | ORDER BY listOrderItem'''
        p[0] = None if len(p) < 4 else p[3]

    def p_listOrderItem(self, p):
        '''listOrderItem : orderItem
                         | orderItem COMA listOrderItem'''
        p[0] = p[3] if len(p) > 2 else deque()
        p[0].appendleft(p[1])

    def p_orderItem(self, p):
        'orderItem : condExpr order nulls'
        p[0] = (AST.Ordering(), p[1], p[2], p[3])

    def p_order(self, p):
        '''order : ASC
                 | DESC
                 | empty'''
        p[0] = p[1] if len(p) > 1 else None

    def p_nulls(self, p):
        '''nulls : NULLS FIRST
                 | NULLS LAST
                 | empty'''
        p[0] = p[2] if len(p) > 2 else None

    def p_empty(self, p):
        'empty :'
        pass

    def p_error(self, token=None):
        if token:
            raise ParserException("Unexpected token: %s" % token)
        else:
            raise ParserException("Unexpected end of input")

    def __init__(self, **kwargs):
        self.parser = yacc.yacc(module=self, **kwargs)

    def parse(self, *args, **kwargs):
        return self.parser.parse(*args, **kwargs)