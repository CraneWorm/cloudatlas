import re
import operator as OP
import parser.visitor as V
from parser import AST
import value
from conversions import Conversions, conversions
import aggretate


class InterpreterException(Exception):
    pass


class RuleNotImplemented(InterpreterException):
    pass


class QueryInterpreter(object):

    def __init__(self):
        pass

    def __call__(self, mainNodeAttributes, childrenNodes, (attrName, strQuery), program):
        self.mainNodeAttributes = mainNodeAttributes
        self.childrenNodes = childrenNodes
        self.backupOfNodeAttributes = mainNodeAttributes.clone()
        if attrName is not None:
            self.mainNodeAttributes.add(attrName, value.ValueQuery(strQuery))
        return self.visit(*program)

    @V.on('node')
    def visit(self, node, *args):
        pass

    @V.when(object)
    def visit(self, node, *args):
        raise RuleNotImplemented("%s has no rule for node of type: %s" % (self.__class__, node.__class__.__name__))

    @V.when(AST.Program)
    def visit(self, node, statements):
        try:
            ret = []
            for statement in statements:
                res = SelectInterpreter()(self.mainNodeAttributes, self.childrenNodes, statement)
                ret.append(res)
                for val, alias in res:
                    assert isinstance(val, value.OneResult), "select must result in single values"
                    if alias is not None:
                        self.mainNodeAttributes.add(alias, val.val)
            return ret
        except Exception, msg:
            print ":: got exception in Program (restore from backup), msg: %s" % (msg)
            self.mainNodeAttributes.update(self.backupOfNodeAttributes)
            raise


class SelectInterpreter(QueryInterpreter):

    def __call__(self, mainNodeAttributes, childrenNodes, query):
        self.mainNodeAttributes = mainNodeAttributes
        self.childrenNodes = childrenNodes
        return self.visit(*query)

    @V.on('node')
    def visit(self, node, *args):
        # TODO: for some mysterious reason this method has to be reimplemented even in subclasses, I might think of a solution later
        pass

    @V.when(AST.Select)
    def visit(self, node, columns, where=None, order_by=None):
        childrenList = WhereInterpreter(self.mainNodeAttributes, self.childrenNodes)(where) if where else self.childrenNodes
        if childrenList and order_by:
            childrenList = OrderByInterpreter(self.mainNodeAttributes, self.childrenNodes, childrenList)(order_by)
        res = SelectListInterpreter(self.mainNodeAttributes, self.childrenNodes, TableEnv(childrenList))(columns)
        return res


class WhereInterpreter(QueryInterpreter):

    def __init__(self, mainNodeAttributes, childrenNodes):
        self.mainNodeAttributes = mainNodeAttributes
        self.childrenNodes = childrenNodes

    def __call__(self, expression):
        return [child for child in self.childrenNodes if ExpressionInterpreter(self.mainNodeAttributes, self.childrenNodes, RowEnv(child))(expression)]

    @V.on('node')
    def visit(self, node, *args):
        pass


class OrderByInterpreter(QueryInterpreter):

    def __init__(self, mainNodeAttributes, childrenNodes, records):
        self.mainNodeAttributes = mainNodeAttributes
        self.childrenNodes = childrenNodes
        self.records = records

    def __call__(self, orderList):
        self.visit(*orderList)
        for order_item in orderList:
            self.visit(*order_item)
        return self.records

    @V.on('node')
    def visit(self, node, *args):
        pass

    @V.when(AST.Ordering)
    def visit(self, node, condExpr, order=None, nulls=None):
        sort_reverse = (order == 'DESC')
        sorting_key = ExpressionInterpreter(self.mainNodeAttributes, self.childrenNodes, TableEnv(self.records))(condExpr)
        assert (len(self.records) == len(sorting_key))  # vital assertion that keeps me sane

        if order == 'DESC':
            asc = False
        else:
            asc = True
        if (nulls == 'FIRST' and asc) or (nulls == 'LAST' and not asc):
            leNone = True
        else:
            leNone = False

        def cmp_none(t1, t2):
            (a, _), (b, _) = t1, t2
            noneA = value.isNone(a)
            noneB = value.isNone(b)
            if noneA and noneB: return 0
            if leNone:
                if noneA: return -1
                if noneB: return 1
            else:
                if noneA: return 1
                if noneB: return -1
            return cmp(a, b)

        _, self.records = zip(*sorted(zip(sorting_key, self.records), reverse=(not asc), cmp=cmp_none))

        assert (len(self.records) == len(sorting_key))  # vital assertion that keeps me even more sane


class SelectListInterpreter(QueryInterpreter):

    def __init__(self, mainNodeAttributes, childrenNodes, env):
        self.mainNodeAttributes = mainNodeAttributes
        self.childrenNodes = childrenNodes
        self.env = env

    def __call__(self, columns):
        return [self.visit(*column) for column in columns]

    @V.on('node')
    def visit(self, node, *args):
        pass

    @V.when(AST.ListSelectItem)
    def visit(self, node, expression, alias=None):
        if self.env.table:
            ex = ExpressionInterpreter(self.mainNodeAttributes, self.childrenNodes, self.env)(expression)
        else:
            ex = value.OneResult(value.ValueNone())
        return (ex, alias)


class ExpressionInterpreter(QueryInterpreter):

    comparison = {
        '=': OP.__eq__, '<>': OP.__ne__, '<': OP.__lt__,
        '>': OP.__gt__, '<=': OP.__le__, '>=': OP.__ge__
    }

    binary_arithmetic = {
        '+': OP.__add__, '*': OP.__mul__,
        '-': OP.__sub__, '/': OP.__div__,
        '%': OP.__mod__
    }

    unary_arithmetic = {
        '-': OP.__neg__
    }

    binary_boolean = {
        'AND': OP.__and__,
        'OR': OP.__or__
    }

    unary_boolean = {
        'NOT': OP.__not__
    }

    def __init__(self, mainNodeAttributes, childrenNodes, env):
        self.mainNodeAttributes = mainNodeAttributes
        self.childrenNodes = childrenNodes
        self.env = env

    def __call__(self, cond):
        return self.visit(*cond)

    @V.on('node')
    def visit(self, node, *args):
        pass

    @V.when(AST.Comparison)
    def visit(self, node, left, right, op):
        lr = self.visit(*left), self.visit(*right)
        return self.comparison[op](*lr)

    @V.when(AST.BinaryArith)
    def visit(self, node, left, right, op):
        lr = self.visit(*left), self.visit(*right)
        return self.binary_arithmetic[op](*lr)

    @V.when(AST.UnaryArith)
    def visit(self, node, right, op):
        r = self.visit(*right)
        return self.unary_arithmetic[op](r)

    @V.when(AST.BinaryBoolean)
    def visit(self, node, left, right, op):
        lr = self.visit(*left), self.visit(*right)
        return self.binary_boolean[op](*lr)

    @V.when(AST.UnaryBoolean)
    def visit(self, node, right, op):
        r = self.visit(*right)
        return self.unary_boolean[op](r)

    @V.when(AST.Value)
    def visit(self, node, val):
        if isinstance(val, str):
            return self.env.get(val)
        return value.OneResult(val)

    @V.when(AST.Select)
    def visit(self, *args):
        results = SelectInterpreter()(self.mainNodeAttributes, self.childrenNodes, args)
        assert len(results) == 1 #srsly, what's the point of multiple expressions in inner select?
        [(value, alias)] = results
        return value

    @V.when(AST.CallExpr)
    def visit(self, node, name, args):
        if name in conversions:
            ex = self.visit(*args[0])
            return Conversions()(name, ex)

        evalExp = lambda x: self.visit(*x)
        return aggretate.AggregateFunction().run(name)(args, evalExp)

    @V.when(AST.RegexpMatch)
    def visit(self, node, expression, regexpr):
        _, reg = regexpr
        regex = re.compile(reg.val)
        result = self.visit(*expression)
        if isinstance(result, value.IterableResult):
            lst = []
            for s in result.val:
                assert isinstance(s, value.ValueString) and s.val != None
                match = regex.match(s.val)
                if match:
                    lst.append(value.ValueBoolean(match.end() == len(s.val)))
                else:
                    lst.append(value.ValueBoolean(False))
            return result.__class__(lst)
        elif isinstance(result, value.OneResult):
            match = regex.match(result.val)
            if match:
                return value.OneResult(value.ValueBoolean(match.end() == len(result.val)))

class RowEnv(object):
    def __init__(self, row):
        self.row = row

    def get(self, ident):
        return value.OneResult(self.row.get(ident))

class TableEnv(object):
    def __init__(self, table):
        self.table = table

    def get(self, ident):
        return value.ColumnResult([row.get(ident) for row in self.table])

