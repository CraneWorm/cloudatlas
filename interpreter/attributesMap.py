import attribute
from interpreter import value

class AttributesMap(object):
    def __init__(self, dict=None, name=None):
        if dict is None:
            self._dict = {}
        else:
            self._dict = dict

    def __repr__(self):
        ret = "\n{\n"
        for key, val in self._dict.iteritems():
            ret += "\t%s: %s\n" % (key, val.val)
        ret += "}"
        return ret

    def __cmp__(self, other):
        return cmp(self._dict, other._dict)

    def update(self, other):
        self._dict.update(other._dict)

    def clone(self):
        attrM = AttributesMap()
        attrM.update(self)
        return attrM

    # def add(self, name, val)
    def add(self, attr, val):
        self._dict[attr] = val

    def remove(self, attr):
        del self._dict[attr]

    # def get(self, name)
    def get(self, attr):
        if attr in self._dict:
            return self._dict[attr]
        else:
            return value.ValueNone()

    def __iter__(self):
        return self._dict.iteritems()
