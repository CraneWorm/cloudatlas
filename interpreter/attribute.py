class Attribute(object):
    def __init__(self, name=None):
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def isQuery(self):
        return self.name.startswith('&')

    def equals(self, rhs):
        if rhs is None:
            return False
        # check classes first?
        return self.name == rhs.name

    def __hash__(self):
        return hash(self._name)
