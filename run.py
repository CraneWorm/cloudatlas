from interpreter.parser.parser import *
from interpreter.parser.lexer import *
from interpreter.interpreter import QueryInterpreter
from interpreter.hierarchy import Hierarchy

import re

class RunInterpreter(object):

    def __init__(self):
        self.parser = Parser(outputdir='tests/interpreter/parser', debug=0)
        self.lexer = Lexer(debug=0)
        self.hierarchy = Hierarchy()
        self.hierarchy.fill()
        self.interpreter = QueryInterpreter()

    def printResult(self, prefix, result):
        if result:
            for select in result:
                for val, name in select:
                    print "%s%s: %s" %(prefix, name, val.val.val)

    def run(self):
        while True:
            try:
                line = raw_input()
                if line:
                    p = re.compile(r'^(&(.*): )?(.*)$')
                    m = p.match(line)
                    if m:
                        attrName = m.group(2)
                        query = m.group(3)
                    else:
                        attrName = None
                        query = line
                    # print "------------\t------------\t------------"
                    print ":: %s: %s" %(attrName, query)
                    self.lexer.input(query)
                    parsed = self.parser.parse(lexer=self.lexer, debug=0)
                    # print "---\t\t---\t\t---\t\t---"
                    result_uw = self.interpreter(self.hierarchy.uw, (attrName, query), parsed)
                    self.printResult('/uw: ', result_uw)
                    result_pjwstk = self.interpreter(self.hierarchy.pjwstk, (attrName, query), parsed)
                    self.printResult('/pjwstk: ', result_pjwstk)
                    result_root = self.interpreter(self.hierarchy.root, (attrName, query), parsed)
                    self.printResult('/: ', result_root)
                else:
                    break
            except (EOFError):
                break
            except Exception, msg:
                print msg
                raise
        # print ":: ZMI uw: " + str(self.hierarchy.uw.attributes)
        # print ":: ZMI pjwstk: " + str(self.hierarchy.pjwstk.attributes)
        # print ":: ZMI root: " + str(self.hierarchy.root.attributes)


def main():
    cmd = RunInterpreter()
    cmd.run()

if __name__ == "__main__":
    main()